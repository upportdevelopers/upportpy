# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
#### Subject:
Upport - Отдел технической поддержки ПО

#### All materials of the project:
https://goo.gl/PxWMnD 
#### Records from the first interview with the customer:
https://goo.gl/EG5aCA
#### Vision and Scope document:
https://goo.gl/SYWb0S
#### Document Supplementary Specification:
https://goo.gl/WlGQaz

### Notes: ###
#### Review of LB 1 and the first stage of LB 2:
https://goo.gl/Kp9L6S
#### Board with User Stories: 
https://nure.storiesonboard.com/m/ipz-ki14-support
#### Project Plan document:
https://goo.gl/A8UZU8
#### Object Model Document:
https://goo.gl/IdP44V

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Костюк Сергей Александрович: serhii.kostiuk@nure.ua

Куликовская Юлия Сергеевна: yuliia.kulykivska@nure.ua

Архиреев Руслан Сергеевич: ruslan.arkhyrieiev@nure.ua 

гр. КИ-14-4, ХНУРЭ
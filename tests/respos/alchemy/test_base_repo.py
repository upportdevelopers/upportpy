# Include standard modules
import unittest
import uuid

# Include 3rd-party modules
from sqlalchemy import Table, Column, Integer, String, MetaData, create_engine
from sqlalchemy.orm import Session, sessionmaker, mapper
from sqlalchemy_utils.types import UUIDType

# Include Upport modules
from upport.model.product import Product
from upport.repos.alchemy.session_manager import SessionManager
from upport.repos.alchemy.repo_impls.product_repo import ProductRepository
from upport.repos.alchemy.meta_builder import MetaBuilder


class TestBaseRepo(unittest.TestCase):
    def test_mess(self):
        mb = MetaBuilder()

        # DO NOT STORE CONNECTION CREDENTIALS WITH YOUR CODE!
        engine = create_engine(
            "mysql+pymysql://pycharm_tester:pycharm_tester@localhost/pycharm_tester_upport?host=localhost?port=3306",
            echo=True
        )

        mb.drop_all_tables(engine)
        mb.create_all_tables(engine)

        session_manager = SessionManager(bind=engine)

        repo = ProductRepository(session_manager)

        test_id = uuid.uuid4()
        test_name = "PyCharm"
        test_page = None

        p = Product(
            domain_id=test_id,
            product_name=test_name,
            product_page=test_page
        )

        self.assertIsNone(
            p.database_id
        )

        repo.add(p)

        self.assertIsNone(
            p.database_id
        )

        repo.commit()

        self.assertIsNotNone(
            p.database_id
        )

        fetched = repo.load_all()

        print(fetched)

        fetched_p = repo.find_by_domain_id(test_id)

        self.assertIs(
            fetched_p, p
        )

        self.assertEqual(
            fetched_p.domain_id, test_id
        )

        self.assertEqual(
            fetched_p.product_page, test_page
        )

        self.assertIsNotNone(
            fetched_p.database_id
        )

        self.assertEqual(
            repo.count(), 1
        )

        all_ids = repo.select_all_domain_ids()

        self.assertEqual(
            all_ids, [test_id]
        )

        repo.delete(fetched_p)

        repo.commit()

        self.assertEqual(
            repo.count(), 0
        )

        self.assertIsNone(
            repo.find_by_domain_id(test_id)
        )

        p = Product(
            domain_id=test_id,
            product_name=test_name,
            product_page=test_page
        )

        repo.add(p)
        repo.rollback()

        self.assertEqual(
            repo.count(), 0
        )


if __name__ == '__main__':
    unittest.main()

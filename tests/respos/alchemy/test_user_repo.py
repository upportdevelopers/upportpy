# Include standard modules
import unittest
import uuid

# Include 3rd-party modules
from sqlalchemy import Table, Column, Integer, String, MetaData, create_engine
from sqlalchemy.orm import Session, sessionmaker, mapper
from sqlalchemy_utils.types import UUIDType

# Include Upport modules
from upport.model.product import Product
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember
from upport.repos.alchemy.repo_impls.user_repo import UserRepository
from upport.repos.alchemy.meta_builder import MetaBuilder
from upport.repos.alchemy.session_manager import SessionManager


class TestUserRepo(unittest.TestCase):
    def test_mess(self):
        mb = MetaBuilder()

        # DO NOT STORE CONNECTION CREDENTIALS WITH YOUR CODE!
        engine = create_engine(
            "mysql+pymysql://pycharm_tester:pycharm_tester@localhost/pycharm_tester_upport?host=localhost?port=3306",
            echo=True
        )

        mb.drop_all_tables(engine)
        mb.create_all_tables(engine)

        session_manager = SessionManager(bind=engine)

        repo = UserRepository(session_manager)

        test_name = "Nikola Tesla"
        test_email = "tesla@gmail.com"
        test_uuid = uuid.uuid4()

        c = Customer(
            domain_id=test_uuid,
            full_name=test_name,
            email=test_email
        )

        test_name2 = "Thomas Edison"
        test_email2 = "edison@gmail.com"
        test_uuid2 = uuid.uuid4()

        sm = StaffMember(
            domain_id=test_uuid2,
            full_name=test_name2,
            email=test_email2,
            position="Chief Engineer"
        )

        repo.add(c)
        repo.add(sm)
        repo.commit()

        del c
        del sm

        self.assertEqual(
            repo.count(), 2
        )

        fetched_c = repo.find_by_email(test_email)

        self.assertTrue(
            isinstance(fetched_c, Customer)
        )

        fetched_sm = repo.find_by_email(test_email2)

        self.assertTrue(
            isinstance(fetched_sm, StaffMember)
        )


if __name__ == '__main__':
    unittest.main()

# Include standard modules
import unittest
import uuid
from datetime import datetime, timedelta
import time

# Include 3rd-party modules
from sqlalchemy import Table, Column, Integer, String, MetaData, create_engine
from sqlalchemy.orm import Session, sessionmaker, mapper
from sqlalchemy_utils.types import UUIDType

# Include Upport modules
from upport.model.request import Request
from upport.model.product_request import ProductRequest
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember
from upport.repos.alchemy.repo_impls.user_repo import UserRepository
from upport.repos.alchemy.repo_impls.request_repo import RequestRepository
from upport.repos.alchemy.meta_builder import MetaBuilder
from upport.repos.alchemy.session_manager import SessionManager


class TestRequestRepo(unittest.TestCase):
    def test_mess(self):
        mb = MetaBuilder()

        # DO NOT STORE CONNECTION CREDENTIALS WITH YOUR CODE!
        engine = create_engine(
            "mysql+pymysql://pycharm_tester:pycharm_tester@localhost/pycharm_tester_upport?host=localhost?port=3306",
            echo=True
        )

        mb.drop_all_tables(engine)
        mb.create_all_tables(engine)

        session_manager = SessionManager(bind=engine)

        repo = UserRepository(session_manager)

        test_name = "Nikola Tesla"
        test_email = "tesla@gmail.com"
        test_uuid = uuid.uuid4()

        c = Customer(
            domain_id=test_uuid,
            full_name=test_name,
            email=test_email
        )

        test_name2 = "Thomas Edison"
        test_email2 = "edison@gmail.com"
        test_uuid2 = uuid.uuid4()

        sm = StaffMember(
            domain_id=test_uuid2,
            full_name=test_name2,
            email=test_email2,
            position="Chief Engineer"
        )

        repo.add(c)
        repo.add(sm)
        repo.commit()

        request_repo = RequestRepository(session_manager)

        r = Request(
            domain_id=uuid.uuid4(),
            creator=c,
            title="Test title 1",
            description="Lorem ipsum dolor sir amet"
        )

        r.assign(sm)

        r2 = Request(
            domain_id=uuid.uuid4(),
            creator=c,
            title="Test title 2",
            description="Duis aute irure dolor in reprehenderit in voluptate "
                        "velit esse cillum dolore eu fugiat nulla pariatur"
        )

        r2.add_related(r)

        r3 = Request(
            domain_id=uuid.uuid4(),
            creator=c,
            title="Test title 3",
            description="Excepteur sint occaecat cupidatat non proident, "
                        "sunt in culpa qui officia deserunt mollit anim id est laborum."
        )

        request_repo.add(r)
        request_repo.add(r2)
        request_repo.add(r3)

        request_repo.commit()

        upd_timestamp = datetime.utcnow() + timedelta(seconds=1)

        time.sleep(2)

        r3.update_title("New test title 3")

        print()
        print(r3.time_updated)
        print(upd_timestamp)
        print()

        session_manager.session.commit()

        results_unassigned = session_manager.session.query(Request).filter_by(_assignee_id=None).all()

        self.assertEqual(
            len(results_unassigned), 2
        )

        like_test = request_repo.search_by_title("New")
        like_est_title = request_repo.search_by_title("est title ")

        self.assertEqual(
            len(like_test), 1
        )

        self.assertEqual(
            len(like_est_title), 3
        )

        results_updated_before = request_repo.select_updated_before(upd_timestamp)
        results_updated_after = request_repo.select_updated_after(upd_timestamp)

        self.assertEqual(
            len(results_updated_before), 2
        )

        self.assertIn(
            r.domain_id, results_updated_before
        )

        self.assertIn(
            r2.domain_id, results_updated_before
        )

        self.assertIn(
            r3.domain_id, results_updated_after
        )

        self.assertEqual(
            len(results_updated_after), 1
        )

        product_request_id = uuid.uuid4()

        product_request = ProductRequest(
            domain_id=product_request_id,
            creator=c,
            title="Test Product Request",
            description="Nothing.\nIs.\nHere."
        )

        request_repo.add(product_request)
        request_repo.commit()

        del product_request

        fetched_pr = request_repo.find_by_domain_id(product_request_id)
        self.assertTrue(
            isinstance(fetched_pr, ProductRequest)
        )


if __name__ == '__main__':
    unittest.main()

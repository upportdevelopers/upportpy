# Include standard modules
import unittest
import uuid
from datetime import datetime, timedelta

# Include 3rd-party modules
from sqlalchemy import Table, Column, Integer, String, MetaData, create_engine
from sqlalchemy.orm import Session, sessionmaker, mapper
from sqlalchemy_utils.types import UUIDType

# Include Upport modules
from upport.model.product import Product
from upport.model.license_type import LicenseType
from upport.model.license import License
from upport.repos.alchemy.repo_impls.product_repo import ProductRepository
from upport.repos.alchemy.repo_impls.license_type_repo import LicenseTypeRepository
from upport.repos.alchemy.repo_impls.license_repo import LicenseRepository
from upport.repos.alchemy.meta_builder import MetaBuilder
from upport.repos.alchemy.session_manager import SessionManager


class TestLicenseRepo(unittest.TestCase):
    def test_mess(self):
        mb = MetaBuilder()

        # DO NOT STORE CONNECTION CREDENTIALS WITH YOUR CODE!
        engine = create_engine(
            "mysql+pymysql://pycharm_tester:pycharm_tester@localhost/pycharm_tester_upport?host=localhost?port=3306",
            echo=True
        )

        mb.drop_all_tables(engine)
        mb.create_all_tables(engine)

        session_manager = SessionManager(bind=engine)

        repo_products = ProductRepository(session_manager)
        repo_license_types = LicenseTypeRepository(session_manager)
        repo_licenses = LicenseRepository(session_manager)

        test_p_id = uuid.uuid4()
        test_p_name = "PyCharm"
        test_p_page = None

        p = Product(
            domain_id=test_p_id,
            product_name=test_p_name,
            product_page=test_p_page
        )

        repo_products.add(p)
        repo_products.commit()

        test_lt_id = uuid.uuid4()
        test_lt_title = "Professional"

        lt = LicenseType(
            domain_id=test_lt_id,
            title=test_lt_title,
            product=p
        )

        repo_license_types.add(lt)
        repo_license_types.commit()

        test_l_id = uuid.uuid4()
        test_l_start = datetime.utcnow()
        test_l_end = test_l_start + timedelta(days=365)

        l = License(
            domain_id=test_l_id,
            license_type=lt,
            date_start=test_l_start,
            date_end=test_l_end,
        )

        repo_licenses.add(l)
        repo_licenses.commit()

        del l

        fetched_l = repo_licenses.find_by_domain_id(test_l_id)

        self.assertEqual(
            fetched_l.domain_id, test_l_id
        )


if __name__ == '__main__':
    unittest.main()

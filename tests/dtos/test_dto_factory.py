# Include standard modules
import unittest
import uuid

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember

from upport.dtos.user_dto import UserDto
from upport.dtos.staff_member_dto import StaffMemberDto

from upport.dtos.dto_factory import build_dto


class TestDtoBuilder(unittest.TestCase):
    test_name = "Nicola Tesla"
    test_email = "tesla@gmail.com"
    test_uuid = uuid.uuid4()
    test_position = "CTO"

    def test_build_customer(self):
        test_customer = Customer(
            domain_id=self.test_uuid,
            full_name=self.test_name,
            email=self.test_email
        )

        result = build_dto(test_customer)

        self.assertTrue(
            isinstance(result, UserDto)
        )

    def test_build_staff_member(self):
        test_sm = StaffMember(
            domain_id=self.test_uuid,
            full_name=self.test_name,
            email=self.test_email,
            position=self.test_position
        )

        result = build_dto(test_sm)

        self.assertTrue(
            isinstance(result, StaffMemberDto)
        )


if __name__ == '__main__':
    unittest.main()

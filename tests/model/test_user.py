# Include standard modules
import unittest
import uuid

# Include 3rd-party modules

# Include Upport modules
from upport.model.user import User, EmptyStringError, InvalidPasswordError, InvalidResetTokenError, PasswordNotSetError


class TestUser(unittest.TestCase):
    test_name = "Nikola Tesla"
    test_email = "tesla@gmail.com"
    test_empty_str = ""
    test_password = "Lorem ipsum dolor sir amet"
    test_uuid = uuid.uuid4()

    def get_base_user(self):
        new_uuid = uuid.uuid4()

        return User(domain_id=new_uuid, full_name=self.test_name, email=self.test_email)

    def setUp(self):
        self.user = self.get_base_user()

        reset_token = self.user.get_password_reset_token()
        self.user.set_password(reset_token, self.test_password)

    def test_init(self):
        user = self.get_base_user()

        self.assertEqual(user.full_name, self.test_name)
        self.assertEqual(user.email, self.test_email)
        self.assertFalse(user.is_password_set)

    def test_init_empty_name(self):
        with self.assertRaises(EmptyStringError):
            User(domain_id=self.test_uuid, full_name=self.test_empty_str, email=self.test_email)

    def test_init_name_not_string(self):
        with self.assertRaises(TypeError):
            User(domain_id=self.test_uuid, full_name=None, email=self.test_email)

    def test_init_empty_email(self):
        with self.assertRaises(EmptyStringError):
            User(domain_id=self.test_uuid, full_name=self.test_name, email=self.test_empty_str)

    def test_init_email_not_string(self):
        with self.assertRaises(TypeError):
            User(domain_id=self.test_uuid, full_name=self.test_name, email=None)

    def test_name_setter(self):
        test_new_name = "Real Nicola Tesla"

        self.user.full_name = test_new_name
        self.assertEqual(self.user.full_name, test_new_name)

    def test_name_setter_empty(self):
        with self.assertRaises(EmptyStringError):
            self.user.full_name = self.test_empty_str

    def test_name_setter_not_str(self):
        with self.assertRaises(TypeError):
            self.user.full_name = None

    def test_email_setter(self):
        test_new_email = "tesla@yahoo.com"

        self.user.email = test_new_email
        self.assertEqual(self.user.email, test_new_email)

    def test_email_setter_empty(self):
        with self.assertRaises(EmptyStringError):
            self.user.email = self.test_empty_str

    def test_email_setter_not_str(self):
        with self.assertRaises(TypeError):
            self.user.email = None

    def test_password_set(self):
        user = self.get_base_user()

        reset_token = user.get_password_reset_token()
        user.set_password(reset_token, self.test_password)

        self.assertTrue(
            user.verify_password(self.test_password)
        )

        self.assertTrue(user.is_password_set)

    def test_password_change(self):
        new_password = "consectetur adipiscing elit"

        self.user.change_password(
            old_password=self.test_password,
            new_password=new_password
        )

        self.assertTrue(
            self.user.verify_password(new_password)
        )

        self.assertTrue(self.user.is_password_set)


if __name__ == '__main__':
    unittest.main()

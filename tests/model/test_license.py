# Include standard modules
import unittest
import uuid
import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.model.license import License
from upport.model.license_type import LicenseType
from upport.model.product import Product
from upport.model.customer import Customer


class TestUser(unittest.TestCase):
    test_date_start = datetime.datetime.utcnow()
    test_duration = datetime.timedelta(days=365)
    test_date_end = test_date_start + test_duration

    def test_init(self):
        customer = Customer(
            domain_id=uuid.uuid4(),
            full_name="Nicola Tesla",
            email="tesla@gmail.com"
        )

        product = Product(
            domain_id=uuid.uuid4(),
            product_name="PyCharm"
        )

        license_type = LicenseType(
            domain_id=uuid.uuid4(),
            title="Professional",
            product=product
        )

        license_ins = License(
            domain_id=uuid.uuid4(),
            license_type=license_type,
            date_start=self.test_date_start,
            date_end=self.test_date_end
        )

        # FIXME: Add more checks of the initial state

        self.assertIsNone(
            license_ins.customer
        )


if __name__ == '__main__':
    unittest.main()

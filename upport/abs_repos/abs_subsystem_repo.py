# Include standard modules
from typing import Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.subsystem import Subsystem
from upport.model.product import Product
from upport.abs_repos.abs_repo import AbsRepository


class AbsSubsystemRepository(AbsRepository[Subsystem]):
    """
    AbsSubsystemRepository is an abstract class that declares a generic
    interface of repository of all subsystems of the products
    """
    def select_by_product(self, product: Product) -> Sequence[UUID]:
        """
        Returns a collection of IDs of subsystems for the specified Product

        :param product: a product which subsystems must be fetched
        :return: a collection of IDs of subsystems for the specified Product
        """
        raise NotImplementedError()

    # FIXME: Consider Change: Add a method for fetching of subsystems by
    # their name

    # FIXME: Consider Change: Add a method for fetching of subsystems by
    # the developer related to it


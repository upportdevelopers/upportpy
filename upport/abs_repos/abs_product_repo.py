# Include standard modules
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.model.product import Product
from upport.abs_repos.abs_repo import AbsRepository


class AbsProductRepository(AbsRepository[Product]):
    """
    AbsProductRepository is an abstract class that declares a generic
    interface of repository of (software) products that are supported
    by User Support Department
    """

    def find_by_name(self, product_name: str) -> Product:
        """
        Returns a first found instance of Product with the specified name

        :param product_name: the name of product to be fetched
        :return: an instance of Product
        """
        raise NotImplementedError()


# Include standard modules
# Include 3rd-party modules

# Include Upport modules
from upport.model.user import User
from upport.abs_repos.abs_repo import AbsRepository


class AbsUserRepository(AbsRepository[User]):
    """
    AbsUserRepository is an abstract class that declares a generic
    interface of repository of users
    """
    def find_by_email(self, email: str) -> User:
        """
        Returns an instance of User by his email

        :param email: email of the user to be found
        :return: an instance of User
        """
        raise NotImplementedError()


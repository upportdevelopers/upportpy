# Include standard modules
from typing import Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.license_type import LicenseType
from upport.model.product import Product
from upport.abs_repos.abs_repo import AbsRepository


class AbsLicenseTypeRepository(AbsRepository[LicenseType]):
    """
    AbsLicenseTypeRepository is an abstract class that declares a generic
    interface of repository of license types
    """

    def select_by_product(self, product: Product) -> Sequence[UUID]:
        """
        Returns a collection of license types on the specified product

        :param product: a product licenses must to be related to
        :return: a collection of licenses for this product
        """
        raise NotImplementedError()



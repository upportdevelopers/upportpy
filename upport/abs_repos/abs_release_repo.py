# Include standard modules
from typing import Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.release import Release
from upport.model.product import Product
from upport.abs_repos.abs_repo import AbsRepository


class AbsReleaseRepository(AbsRepository[Release]):
    """
    AbsReleaseRepository is an abstract class that declares a generic
    interface of repository of all released of the Product
    """
    def select_by_product(self, product: Product) -> Sequence[UUID]:
        """
        Returns a collection of IDs of releases for the specified Product

        :param product: a product which releases must be fetched
        :return: a collection of IDs of releases for the specified Product
        """
        raise NotImplementedError()

    def find_by_version(self, version: str) -> Release:
        """
        Returns the first found release with the version specified

        :param version: the version of release to be found
        :return: an instance of Release which corresponds to the
                 specified product version
        """
        raise NotImplementedError()


# Include standard modules
from typing import TypeVar, Generic, Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity


TEntity = TypeVar("TEntity", bound=BaseEntity)


class AbsRepository(Generic[TEntity]):
    """
    AbsRepository is an abstract class that declares a generic
    repository interface
    """
    def count(self) -> int:
        """
        Returns a number of instances stored in repository

        :return: int, a number of instances
        """
        raise NotImplementedError()

    def load(self, db_id: int) -> TEntity or None:
        """
        Loads an object with the specified database identifier
        to memory

        :param db_id: int, a database identifier of object
        :return: an instance of object with the corresponding database ID
                 or None if not found
        """
        raise NotImplementedError()

    def load_all(self) -> Sequence[TEntity]:
        """
        Returns a collection of all objects stored

        :return: a collection of stored objects
        """
        raise NotImplementedError()

    def find_by_domain_id(self, domain_id: UUID) -> TEntity or None:
        """
        Returns an Entity instance by the specified
        domain ID of this instance

        :param domain_id: a domain ID of Entity to be found
        :return: an instance of Entity with the specified ID
                 or None if it was not found
        """
        raise NotImplementedError()

    def select_all_domain_ids(self) -> Sequence[UUID]:
        """
        Returns a collection of domain identifiers of all
        Entity instances stored in repository

        :return: a collection of domain identifiers
        """
        raise NotImplementedError()

    def add(self, new_obj: TEntity) -> None:
        """
        Add a new object to the repository

        :param new_obj: new object to be stored
        :return: None
        """
        raise NotImplementedError()

    def delete(self, obj: TEntity) -> None:
        """
        Deletes the object specified from the repository

        :param obj: an object to be deleted
        :return: None
        """
        raise NotImplementedError()

    # TODO: Add a context manager for transactions
    def start_transaction(self) -> None:
        """
        Starts a transaction - a set of related changes in
        persistent storage

        :return: None
        """
        raise NotImplementedError()

    def commit(self) -> None:
        """
        Save all made changes to the persistent storage

        :return: None
        """
        raise NotImplementedError()

    def rollback(self) -> None:
        """
        Rollbacks the current transaction

        :return: None
        """
        raise NotImplementedError()


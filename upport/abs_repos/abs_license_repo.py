# Include standard modules
from typing import Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.license import License
from upport.model.license_type import LicenseType
from upport.model.customer import Customer
from upport.abs_repos.abs_repo import AbsRepository


class AbsLicenseRepository(AbsRepository[License]):
    """
    AbsLicenseRepository is an abstract class that declares a generic
    interface of repository of licenses
    """
    def select_by_user(self, customer: Customer) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all licenses that are given to the
        specified Customer

        :param customer: a customer to be checked
        :return: a collection of License IDs that belongs to this Customer
        """
        raise NotImplementedError()

    def select_unused(self) -> Sequence[UUID]:
        """
        Returns a collection of IDs of licenses that are not yet associated to
        or activated by any Customer

        :return: a collection of IDs of unused licenses
        """
        raise NotImplementedError()

    def select_by_type(self, license_type: LicenseType) -> Sequence[UUID]:
        """
        Returns a collection of IDs of licenses of the specific type

        :param license_type: license type of Licenses to be fetched
        :return: a collection of IDs of licenses of the specified type
        """
        raise NotImplementedError()



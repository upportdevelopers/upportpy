# Include standard modules
from typing import Sequence
from datetime import datetime
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.request import Request
from upport.model.user import User
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember
from upport.model.product import Product
from upport.model.subsystem import Subsystem
from upport.model.release import Release
from upport.model.product_request import ProductRequest
from upport.model.ib_request import IBRequest
from upport.abs_repos.abs_repo import AbsRepository


class AbsRequestRepository(AbsRepository[Request]):
    """
    AbsRequestRepository is an abstract class that declares a generic
    interface of repository of requests (including its subtypes)
    """
    def select_by_user(self, user: User) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests that are related to the
        specified user (he or she was a creator of this request or
        its assignee.

        :param user: an instance of user, a creator or assignee of
               requests returned
        :return: a collection of IDs of Requests that are related to User
        """
        raise NotImplementedError()

    def select_by_creator(self, creator: Customer) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests which was created by the
        specified customer.

        :param creator: an instance of Customer which crated the requests
               to be returned
        :return: a collection of IDs of Requests that was created to this Customer
        """
        raise NotImplementedError()

    def select_by_assignee(self, assignee: StaffMember) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests that are assigned to the
        specified StaffMember

        :param assignee: an instance of StaffMember which is an assignee of
               the requests to be returned
        :return: a collection of IDs of Requests that was assigned to this StaffMember
        """
        raise NotImplementedError()

    def select_unassigned_requests(self) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests that wasn't assigned to any
        StaffMember yet

        :return: a collection of Requests without an assignee
        """
        raise NotImplementedError()

    def select_updated_before(self, before: datetime) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests that were updated before the
        specified point of time

        Is useful for a detection of abandoned requests.

        :param before: the upper bound of time_updated field
        :return: a collection of IDs of requests that were updated before the
                 specified point of time
        """
        raise NotImplementedError()

    def select_updated_after(self, after: datetime) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests that were updated after the
        specified point of time

        :param after: the lower bound of time_updated field
        :return: a collection of IDs of requests that were updated after the
                 specified point of time
        """
        raise NotImplementedError()

    def select_updated_between(self, after: datetime, before: datetime) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all requests that were updated between
        two points in time

        :param after: the lower bound of time_updated field
        :param before: the upper bound of time_updated field
        :return: a collection of IDs of requests that were updated between
                 the specified points of time
        """
        raise NotImplementedError()

    def select_by_product(self, product: Product) -> Sequence[ProductRequest]:
        """
        Returns a collection of IDs of all requests that are related to the
        specified product

        :param product: a product that all returned requests must be
               related to
        :return: a collection of IDs of requests that are related to the
                 specified product
        """
        raise NotImplementedError()

    def select_by_release(self, release: Release) -> Sequence[ProductRequest]:
        """
        Returns a collection of IDs of all requests that are related to the
        specified release of the product

        :param release: a release that all returned requests must be
               related to
        :return: a collection of IDs of requests that are related to the
                 specified product release
        """
        raise NotImplementedError()

    def select_by_subsystem(self, subsystem: Subsystem) -> Sequence[IBRequest]:
        """
        Returns a collection of IDs of all requests that are related to the
        specified subsystem of the product. Returns IBRequests only.

        :param subsystem: a subsystem that all returned requests must
               be related to
        :return: a collection of IDs of requests that are related to the
                 specified product subsystem
        """
        raise NotImplementedError()

    def search_by_title(self, search_string: str) -> Sequence[UUID]:
        """
        Allows to get a full list of request whose titles contain the
        specified search string

        :param search_string: a title search string
        :return: a collection of Requests which contain the specified
                 string in their titles
        """
        raise NotImplementedError()
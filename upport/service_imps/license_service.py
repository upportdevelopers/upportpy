# Include standard modules
from typing import Sequence
import uuid
from datetime import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.abs_repos.abs_license_type_repo import AbsLicenseTypeRepository
from upport.abs_repos.abs_license_repo import AbsLicenseRepository
from upport.abs_repos.abs_user_repo import AbsUserRepository
from upport.abs_repos.abs_product_repo import AbsProductRepository
from upport.abs_services.abs_license_service import AbsLicenseService
from .base_service import BaseService

from upport.dtos.license_dto import LicenseDto
from upport.dtos.dto_factory import build_dto

from upport.model.license_type import LicenseType
from upport.model.license import License
from upport.model.customer import Customer


class LicenseService(AbsLicenseService, BaseService):
    """
    An implementation of AbsLicenseService
    """

    def __init__(self, license_repo: AbsLicenseRepository,
                 license_type_repo: AbsLicenseTypeRepository,
                 product_repo: AbsProductRepository,
                 user_repo: AbsUserRepository):
        """
        Initialize LicenseService and save links to its dependencies
        internally

        :param license_repo: an instance of AbsLicenseRepository
        :param license_type_repo: an instance of AbsLicenseTypeRepository
        :param user_repo: an instance of AbsUserRepository
        """
        assert isinstance(license_repo, AbsLicenseRepository)
        assert isinstance(license_type_repo, AbsLicenseTypeRepository)
        assert isinstance(product_repo, AbsProductRepository)
        assert isinstance(user_repo, AbsUserRepository)

        self._licenses = license_repo
        self._license_types = license_type_repo
        self._products = product_repo
        self._users = user_repo

    def view_all(self) -> Sequence[uuid.UUID]:
        """
        Fetch a full list of identifiers of stored objects

        :return: a collection of uuid.UUIDs
        """
        return self._licenses.select_all_domain_ids()

    def view(self, domain_id: uuid.UUID) -> LicenseDto:
        """
        Fetch a DTO of stored object by the ID specified

        :param domain_id: id of object to be fetched
        :return: a DTO of stored object
        """
        license_ins = self._resolve_entity(self._licenses, domain_id)
        dto = build_dto(license_ins)  # type: LicenseDto

        assert isinstance(dto, LicenseDto)

        return dto

    def create_license(self, for_user: uuid.UUID, of_type: uuid.UUID, from_date: datetime, to_date: datetime) -> uuid.UUID:
        """
        Allows to create a new instance of License for User

        :param for_user: an identifier of Customer which will use the new license
        :param of_type: an identifier of LicenseType
        :param from_date: license activity start date
        :param to_date: a date of expiration
        :return: an identifier of the created License
        """
        self._licenses.start_transaction()

        license_user = self._resolve_entity(self._users, for_user)
        license_type = self._resolve_entity(self._license_types, of_type)

        new_license = License(
            domain_id=uuid.uuid4(),
            license_type=license_type,
            date_start=from_date,
            date_end=to_date
        )

        new_license.customer = license_user

        self._licenses.add(new_license)

        self._licenses.commit()

        return new_license.domain_id

    def prolong_license(self, domain_id: uuid.UUID, until: datetime) -> None:
        """
        Allows to postpone a license expiration date

        :param domain_id: an ID of License to be altered
        :param until: a new expiration date in future
        :return: None
        """
        self._licenses.start_transaction()

        license_ins = self._resolve_entity(self._licenses, domain_id)
        license_ins.prolong(until=until)

        self._licenses.commit()

    def revoke_license(self, domain_id: uuid.UUID) -> None:
        """
        Allows to revoke the specified license

        :param domain_id: an ID of License to be revoked
        :return: None
        """
        self._licenses.start_transaction()

        license_ins = self._resolve_entity(self._licenses, domain_id)
        license_ins.invalidate_license()

        self._licenses.commit()

    def fetch_by_user(self, customer_id: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a list of all IDs of Licenses that are used by the
        specified customer

        :param customer_id: an ID of customer to be fetched by
        :return: a collection of IDs of Licenses that are used by Customer
        """
        user = self._resolve_entity(self._users, customer_id)  # type: Customer

        if not isinstance(user, Customer):
            return list()

        licenses = self._licenses.select_by_user(user)

        return licenses

    def view_by_product(self, product_id: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a list of all IDs of Licenses that are related to the
        specified Product

        :param product_id: an identifier of Product which Licenses must to belong to
        :return: a list of IDs of issued Licenses for on Product
        """
        product = self._resolve_entity(self._products, product_id)
        license_types = self._license_types.select_by_product(product)  # type: Sequence[uuid.UUID]

        result = []

        for lt_id in license_types:
            lt = self._resolve_entity(self._license_types, lt_id)
            licenses = self._licenses.select_by_type(lt)

            result.extend(
                licenses
            )

        return result

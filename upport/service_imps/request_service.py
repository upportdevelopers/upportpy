# Include standard modules
from typing import Sequence, List
import uuid
from datetime import datetime, timedelta

# Include 3rd-party modules

# Include Upport modules
from upport.abs_repos.abs_request_repo import AbsRequestRepository
from upport.abs_repos.abs_user_repo import AbsUserRepository
from upport.abs_services.abs_request_service import AbsRequestService
from .base_service import BaseService

from upport.dtos.request_dto import RequestDto
from upport.dtos.response_dto import ResponseDto
from upport.dtos.dto_factory import build_dto

from upport.model.response import Response
from upport.model.request import Request, RequestStatus
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember


def return_24_hours_ago():
    delta = timedelta(days=1)
    return datetime.utcnow() - delta


def return_month_ago():
    delta = timedelta(days=30)
    return datetime.utcnow() - delta


class RequestService(AbsRequestService, BaseService):
    """
    An implementation of AbsRequestService
    """

    def __init__(self, request_repo: AbsRequestRepository, user_repo: AbsUserRepository):
        """
        Initialize RequestService and save links to its dependencies
        internally

        :param request_repo: an instance of AbsRequestRepository
        :param user_repo: an instance of AbsUserRepository
        """
        assert isinstance(request_repo, AbsRequestRepository)
        assert isinstance(user_repo, AbsUserRepository)
        
        self._requests = request_repo
        self._users = user_repo
        
    def view_all(self) -> Sequence[uuid.UUID]:
        """
        Fetch a full list of identifiers of stored objects

        :return: a collection of uuid.UUIDs
        """
        return self._requests.select_all_domain_ids()

    def view(self, domain_id: uuid.UUID) -> RequestDto:
        """
        Fetch a DTO of stored object by the ID specified

        :param domain_id: id of object to be fetched
        :return: a DTO of stored object
        """
        request = self._resolve_entity(self._requests, domain_id)
        dto = build_dto(request)  # type: RequestDto

        assert isinstance(dto, RequestDto)

        return dto
    
    def create_request(self, creator_id: uuid.UUID, title: str, description: str) -> uuid.UUID:
        """
        Creates a new request in the system with the specified author (creator),
        title and description.

        :param creator_id: an identifier of creator of this request
        :param title: a title of request
        :param description: a description of request
        :return: an ID of the created request
        """
        creator = self._resolve_entity(self._users, creator_id)  # type: Customer

        self._requests.start_transaction()

        new_request = Request(
            domain_id=uuid.uuid4(),
            creator=creator,
            title=title,
            description=description
        )
        self._requests.add(new_request)

        self._requests.commit()

        return new_request.domain_id

    def assign_request(self, domain_id: uuid.UUID, assignee_id: uuid.UUID) -> None:
        """
        Allows to set a new assignee of the request specified

        :param domain_id: an ID of request to be edited
        :param assignee_id: an ID of the new assignee to be set
        :return: None
        """
        self._requests.start_transaction()

        user = self._resolve_entity(self._users, assignee_id)  # type: StaffMember

        request = self._resolve_entity(self._requests, domain_id)
        request.assign(user)

        self._requests.commit()

    def change_status(self, domain_id: uuid.UUID, new_status: str) -> None:
        """
        Allows to set a new status of the request

        :param domain_id: an ID of request to be edited
        :param new_status: a string representation of the new state
        :return: None
        """
        self._requests.start_transaction()

        status = getattr(RequestStatus, new_status)
        request = self._resolve_entity(self._requests, domain_id)
        request.status = status

        self._requests.commit()

    def change_title(self, domain_id: uuid.UUID, new_title: str) -> None:
        """
        Allows to set a new title of the request

        :param domain_id: an ID of request to be edited
        :param new_title: a new title to be set
        :return: None
        """
        self._requests.start_transaction()

        request = self._resolve_entity(self._requests, domain_id)
        request.update_title(new_title)

        self._requests.commit()

    def change_description(self, domain_id: uuid.UUID, new_description: str) -> None:
        """
        Allows to set a new description of the request

        :param domain_id: an ID of request to be edited
        :param new_description: a new description to be set
        :return: None
        """
        self._requests.start_transaction()

        request = self._resolve_entity(self._requests, domain_id)
        request.update_description(new_description)

        self._requests.commit()

    # FIXME: Consider Change: Return something else or None instead of uuid.UUID
    def post_response(self, to_request: uuid.UUID, creator_id: uuid.UUID, body: str) -> uuid.UUID:
        """
        Creates a new response in the request specified

        :param to_request: a request this response is related to
        :param creator_id: an identifier of response creator (User)
        :param body: a body of response
        :return: an identifier of RESPONSE in this request
        """
        self._requests.start_transaction()

        request = self._resolve_entity(self._requests, to_request)
        user = self._resolve_entity(self._users, creator_id)

        response = Response(
            domain_id=uuid.uuid4(),
            creator=user,
            body=body
        )

        request.add_response(response)

        self._requests.commit()

        return response.domain_id

    def add_related(self, domain_id: uuid.UUID, new_related: uuid.UUID) -> None:
        """
        Adds a new request with the new_related ID to the request with
        the domain_id specified

        :param domain_id: an acceptor of related request
        :param new_related: a request to be added
        :return: None
        """
        self._requests.start_transaction()

        target = self._resolve_entity(self._requests, domain_id)
        related_request = self._resolve_entity(self._requests, new_related)

        target.add_related(related_request)

        self._requests.commit()

    def remove_related(self, domain_id: uuid.UUID, on_removal: uuid.UUID) -> None:
        """
        Removes a request with the on_removal ID from the request with
        the domain_id specified

        :param domain_id: an acceptor of related request
        :param on_removal: a request to be removed from a list of related
        :return: None
        """
        self._requests.start_transaction()

        target = self._resolve_entity(self._requests, domain_id)
        related_request = self._resolve_entity(self._requests, on_removal)

        target.remove_related(related_request)

        self._requests.commit()

    # FIXME: Consider Change: Remove this method
    def fetch_response(self, in_request: uuid.UUID, domain_id: uuid.UUID) -> ResponseDto or None:
        """
        Allows to fetch a specific response with identifier domain_id from
        the request with identifier in_request

        :param in_request: an ID of the Request
        :param domain_id: an ID of the response itself
        :return: RequestDto
        """
        request = self._resolve_entity(self._requests, in_request)

        result = None

        for r in request.responses:
            if r.domain_id == domain_id:
                result = r

        if result is None:
            return None

        dto = build_dto(result)  # type: ResponseDto

        return dto

    def fetch_all_responses(self, in_request: uuid.UUID) -> Sequence[ResponseDto]:
        """
        Allows to fetch all responses in the request specified

        :param in_request: request the responses are related to
        :return: a collection of ResponseDto
        """
        request = self._resolve_entity(self._requests, in_request)

        result = [build_dto(r) for r in request.responses]

        return result

    # FIXME: Consider Change: Remove this method
    def fetch_by_related_request(self, to_request: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a collection of DTOs of all requests related to the specified one

        :param to_request: a request ID by which the selection is performed
        :return: a collection of IDs of related requests
        """
        source_request = self._resolve_entity(self._requests, to_request)

        result = [r.domain_id for r in source_request.related_requests]

        return result

    def fetch_by_user(self, user_id: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a collection of identifiers of all requests that are related to
        the specified user (are assigned to it or was created by it)

        :param user_id: an ID of the user which is related to the returned requests
        :return: a collection of IDs of Requests that are related to this User
        """
        user = self._resolve_entity(self._users, user_id)
        requests = self._requests.select_by_user(user)

        return requests

    def fetch_created(self, by_user: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a collection of identifiers of all requests that are assigned to
        the specified user

        :param by_user: an ID of the creator of requests to be fetched
        :return: a collection of IDs of Requests that are created by this User
        """
        user = self._resolve_entity(self._users, by_user)  # type: Customer
        requests = self._requests.select_by_creator(user)

        return requests

    def fetch_assigned(self, to_user: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a collection of identifiers of all requests that are assigned to
        the specified user

        :param to_user: an ID of the assignee who created requests to be fetched
        :return: a collection of IDs of Requests that are assigned this User
        """
        user = self._resolve_entity(self._users, to_user)  # type: StaffMember
        requests = self._requests.select_by_assignee(user)

        return requests

    def view_unassigned_requests(self) -> Sequence[uuid.UUID]:
        """
        Allows to get a full list of requests that haven't an assignee yet

        :return: a collection of IDs of unassigned Requests
        """
        requests = self._requests.select_unassigned_requests()

        return requests

    def view_recently_updated_requests(self) -> Sequence[uuid.UUID]:
        """
        Allows to get a full list of requests that were updated for the last 24 hours

        :return: a collection of IDs of recently updated Requests
        """
        requests = self._requests.select_updated_after(return_24_hours_ago())

        return requests

    def view_abandoned_requests(self) -> Sequence[uuid.UUID]:
        """
        Allows to get a full list of requests that aren't closed yet
        and was updated more than a month ago

        :return: a collection of IDs of abandoned Requests
        """
        requests = self._requests.select_updated_before(return_month_ago())

        return requests

    def search_by_title(self, search_string: str) -> Sequence[uuid.UUID]:
        """
        Allows to get a full list of request whose titles contain the
        specified search string

        :param search_string: a title search string
        :return: a collection of IDs of Requests which contain the
                 specified string in their titles
        """
        requests = self._requests.search_by_title(search_string)

        return requests

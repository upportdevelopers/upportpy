from uuid import UUID

from upport.model.base_entity import BaseEntity
from upport.abs_repos.abs_repo import AbsRepository


class UnresolvedEntityError(RuntimeError):
    """
    Exception to be raised if the object with the
    specified ID can't be fetched (resolved)
    """
    pass


class BaseService(object):
    """
    A base class for all service implementations.
    Provides only two private methods: resolve_entity
    and check_resolved
    """
    def _resolve_entity(self, repository: AbsRepository, domain_id: UUID):
        """
        Resolves an entity object by the specified ID

        :param repository: a source container for resolving
        :param domain_id: ID of object to be fetched
        :return: an instance of BaseEntity from the repository
        """
        entity = repository.find_by_domain_id(domain_id)

        self._check_resolved(entity)

        return entity

    @staticmethod
    def _check_resolved(resolved: BaseEntity) -> None:
        """
        Checks that the resolved entity is not None
        (i.e. that the entity was found)

        :param resolved: an instance of entity object to be checked
        :return: None
        :raises UnresolvedEntityError: if the specified object
                is None
        """
        if resolved is None:
            raise UnresolvedEntityError()

# Include standard modules
import uuid
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.abs_repos.abs_product_repo import AbsProductRepository
from upport.abs_services.abs_product_service import AbsProductService
from .base_service import BaseService
from upport.dtos.product_dto import ProductDto
from upport.dtos.dto_factory import build_dto

from upport.model.product import Product


class ProductService(AbsProductService, BaseService):
    """
    An implementation of AbsProductService
    """

    def __init__(self, product_repo: AbsProductRepository):
        """
        Initialize ProductService and save links to its dependencies
        internally

        :param product_repo: an instance of AbsProductRepository
        """
        assert isinstance(product_repo, AbsProductRepository)

        self._products = product_repo

    def view_all(self) -> Sequence[uuid.UUID]:
        """
        Fetch a full list of identifiers of stored objects

        :return: a collection of UUIDs
        """
        return self._products.select_all_domain_ids()

    def view(self, domain_id: uuid.UUID) -> ProductDto:
        """
        Fetch a DTO of stored object by the ID specified

        :param domain_id: id of object to be fetched
        :return: a DTO of stored object
        """
        product = self._resolve_entity(self._products, domain_id)
        dto = build_dto(product)  # type: ProductDto

        assert isinstance(dto, ProductDto)

        return dto

    def create_product(self, product_name: str, product_page: str or None) -> uuid.UUID:
        """
        Allows to create a new instance of product

        :param product_name: the name of new product
        :param product_page: the product page, may be null (None)
        :return: an identifier of created product
        """
        self._products.start_transaction()

        new_product = Product(
            domain_id=uuid.uuid4(),
            product_name=product_name,
            product_page=product_page
        )

        self._products.add(new_product)

        self._products.commit()

        return new_product.domain_id

    def set_product_page(self, domain_id: uuid.UUID, new_page: str or None) -> None:
        """
        Allows to set a new product page or clean the value of this field

        :param domain_id: an ID of Product to be modified
        :param new_page: a URL to the product new page to be set
        :return: None
        """

        product = self._resolve_entity(self._products, domain_id)

        self._products.start_transaction()

        product.product_page = new_page

        self._products.commit()

    def fetch_by_name(self, product_name: str) -> ProductDto:
        """
        Returns a first found instance of Product DTO that has the specified
        name.

        :param product_name: a name of the product to be found
        :return: an instance of Product DTO
        """
        product = self._products.find_by_name(product_name)
        self._check_resolved(product)
        dto = build_dto(product)  # type: ProductDto

        assert isinstance(dto, ProductDto)

        return dto


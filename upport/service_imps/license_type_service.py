# Include standard modules
import uuid
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.abs_repos.abs_license_type_repo import AbsLicenseTypeRepository
from upport.abs_repos.abs_product_repo import AbsProductRepository
from upport.abs_services.abs_license_type_service import AbsLicenseTypeService
from .base_service import BaseService

from upport.dtos.license_type_dto import LicenseTypeDto
from upport.dtos.dto_factory import build_dto

from upport.model.license_type import LicenseType


class LicenseTypeService(AbsLicenseTypeService, BaseService):
    """
    An implementation of AbsLicenseTypeService
    """

    def __init__(self, license_type_repo: AbsLicenseTypeRepository, product_repo: AbsProductRepository):
        """
        Initialize LicenseTypeService and save links to its dependencies
        internally

        :param license_type_repo: an instance of AbsLicenseTypeRepository
        :param product_repo: an instance of AbsProductRepository
        """
        assert isinstance(license_type_repo, AbsLicenseTypeRepository)
        assert isinstance(product_repo, AbsProductRepository)

        self._license_types = license_type_repo
        self._products = product_repo

    def view_all(self) -> Sequence[uuid.UUID]:
        """
        Fetch a full list of identifiers of stored objects

        :return: a collection of UUIDs
        """
        return self._products.select_all_domain_ids()

    def view(self, domain_id: uuid.UUID) -> LicenseTypeDto:
        """
        Fetch a DTO of stored object by the ID specified

        :param domain_id: id of object to be fetched
        :return: a DTO of stored object
        """
        license_type = self._resolve_entity(self._license_types, domain_id)
        dto = build_dto(license_type)  # type: LicenseTypeDto

        assert isinstance(dto, LicenseTypeDto)

        return dto

    def view_by_product(self, product_id: uuid.UUID) -> Sequence[uuid.UUID]:
        """
        Returns a collection of LicenseTypes IDs for the specified
        product

        :param product_id: a product the result types must be related to
        :return: a collection of LicenseType IDs
        """
        product = self._resolve_entity(self._products, product_id)
        license_types = self._license_types.select_by_product(product)

        return license_types

    def create_license_type(self, title: str, product_id: uuid.UUID) -> uuid.UUID:
        """
        Allows to create a new instance of LicenseType

        :param title: the name of new LicenseType
        :param product_id: an identifier of Product the new LicenseType
               must be related to
        :return: an identifier of the created LicenseType
        """
        self._license_types.start_transaction()

        product = self._resolve_entity(self._products, product_id)
        new_license_type = LicenseType(
            domain_id=uuid.uuid4(),
            title=title,
            product=product
        )

        self._license_types.add(new_license_type)

        self._license_types.commit()

        return new_license_type.domain_id

    def set_description(self, domain_id: uuid.UUID, new_description: str or None) -> None:
        """
        Allows to set, unset or change the description of this LicenseType

        :param domain_id: an ID of the LicenseType to be edited
        :param new_description: a new value of description field
        :return: None
        """
        license_type = self._resolve_entity(self._license_types, domain_id)

        self._license_types.start_transaction()

        license_type.description = new_description

        self._license_types.commit()

# Include standard modules
import uuid
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.abs_repos.abs_user_repo import AbsUserRepository
from upport.abs_services.abs_user_service import AbsUserService
from .base_service import BaseService, UnresolvedEntityError
from upport.dtos.user_dto import UserDto
from upport.dtos.dto_factory import build_dto

from upport.model.user import User
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember


class UnauthorizedError(PermissionError):
    """
    Indicates that the user was not authorized
    """
    pass


class EmailAlreadyRegisteredError(PermissionError):
    """
    Indicates that the user with the specified email already exists
    """
    pass


class NotStaffMemberError(ValueError):
    """
    Indicates that the user with the ID is not a StaffMember
    """
    pass


class UserService(AbsUserService, BaseService):
    """
    An implementation of UserService
    """

    def __init__(self, user_repo: AbsUserRepository):
        """
        Initialize UserService and save links to its dependencies
        internally

        :param user_repo: an instance of AbsUserRepository
        """
        assert isinstance(user_repo, AbsUserRepository)

        self._users = user_repo

    def view_all(self) -> Sequence[uuid.UUID]:
        """
        Fetch a full list of identifiers of stored objects

        :return: a collection of UUIDs
        """
        return self._users.select_all_domain_ids()

    def view(self, domain_id: uuid.UUID) -> UserDto:
        """
        Fetch a DTO of stored object by the ID specified

        :param domain_id: id of object to be fetched
        :return: a DTO of stored object
        """
        user = self._resolve_entity(self._users, domain_id)
        dto = build_dto(user)  # type: UserDto

        assert isinstance(dto, UserDto)

        return dto

    def identify(self, email: str, password: str) -> UserDto:
        """
        Check if user-password combination is valid an fetch
        a DTO of the corresponding user

        :param email: an email of the user to be identified
        :param password: a password of the user
        :return: an instance of UserDto
        """
        user = self._resolve_by_email(email)

        if not user.verify_password(password):
            raise UnauthorizedError()

        dto = build_dto(user)  # type: UserDto
        return dto

    def change_full_name(self, user_id: uuid.UUID, new_name: str) -> None:
        """
        Changes a full name of the specified User

        :param user_id: -//-
        :param new_name: -//-
        :return: None
        """
        user = self._resolve_entity(self._users, user_id)

        self._users.start_transaction()
        user.full_name = new_name
        self._users.commit()

    def change_email(self, user_id: uuid.UUID, new_email: str) -> None:
        """
        Changes an email of the specified User

        :param user_id: -//-
        :param new_email: -//-
        :return: None
        """
        self._check_not_registered(email=new_email)

        user = self._resolve_entity(self._users, user_id)

        self._users.start_transaction()
        user.email = new_email
        self._users.commit()

    def change_password(self, user_id: uuid.UUID, old_password: str, new_password: str) -> None:
        """
        Changes a password of the specified User

        :param user_id: -//-
        :param old_password: -//-
        :param new_password: -//-
        :return: None
        """
        user = self._resolve_entity(self._users, user_id)

        self._users.start_transaction()
        user.change_password(old_password, new_password)
        self._users.commit()

    def get_passwd_reset_token(self, email: str) -> str:
        """
        Allows to fetch a password reset token for the specified User

        :param email: an email of the user
        :return: string, a password reset token
        """
        user = self._resolve_by_email(email)

        self._users.start_transaction()
        token = user.get_password_reset_token()
        self._users.commit()

        return token

    def reset_password(self, email: str, reset_token: str, new_password: str) -> None:
        """
        Allows to reset a current password for the specified User if
        the specified password reset token is valid

        :param email: an email of the user
        :param reset_token: a reset token
        :param new_password: a new password to be set
        :return: None
        """
        user = self._resolve_by_email(email)

        self._users.start_transaction()
        user.set_password(reset_token, new_password)
        self._users.commit()

    def _check_not_registered(self, email: str) -> None:
        """
        Checks if the User with the specified email was not already registered

        :param email: email of the user
        :return: None
        """
        if self._users.find_by_email(email) is not None:
            raise EmailAlreadyRegisteredError()

    def create_customer(self, full_name: str, email: str) -> uuid.UUID:
        """
        Allows to create a new Customer in the system. To pick a
        password, the customer must to perform the password resetting
        procedure

        :param full_name: a full name of the customer
        :param email: an email of the customer
        :return: an ID of the created customer
        """
        self._check_not_registered(email)

        self._users.start_transaction()

        new_user = Customer(
            uuid.uuid4(),
            full_name,
            email
        )
        self._users.add(new_user)

        self._users.commit()

        return new_user.domain_id

    def create_staff_member(self, full_name: str, email: str, position: str) -> uuid.UUID:
        """
        Allows to create a new StaffMember in the system. To pick a
        password, the StaffMember must to perform the password resetting
        procedure.

        :param full_name: a full name of the customer
        :param email: an email of the customer
        :param position: a position of the staff member in the department
        :return: an ID of the created customer
        """
        self._check_not_registered(email)

        self._users.start_transaction()

        new_user = StaffMember(
            uuid.uuid4(),
            full_name,
            email,
            position
        )
        self._users.add(new_user)

        self._users.commit()

        return new_user.domain_id

    def activate_staff_account(self, domain_id: uuid.UUID) -> None:
        """
        Allows to make a StaffMember account active, i.e. available for
        logging in (if password was previously set, of course)

        :param domain_id: an ID of StaffMember
        :return: None
        """
        user = self._resolve_entity(self._users, domain_id)

        if not isinstance(user, StaffMember):
            raise NotStaffMemberError()

        self._users.start_transaction()
        user.activate()
        self._users.commit()

    def deactivate_staff_account(self, domain_id: uuid.UUID) -> None:
        """
        Allows to make a StaffMember account inactive, i.e. unavailable for
        logging in (either the password was previously set or not).
        At the same time the password setting doesn't change.

        :param domain_id: an ID of StaffMember
        :return: None
        """
        user = self._resolve_entity(self._users, domain_id)

        if not isinstance(user, StaffMember):
            raise NotStaffMemberError()

        self._users.start_transaction()
        user.deactivate()
        self._users.commit()

    def change_staff_account_position(self, user_id: uuid.UUID, new_position: str) -> None:
        """
        Sets a new position for the specified StaffMember

        :param user_id: an ID of the StaffMember to be changed
        :param new_position: a new position to be set
        :return: None
        """
        user = self._resolve_entity(self._users, user_id)

        if not isinstance(user, StaffMember):
            raise NotStaffMemberError()

        self._users.start_transaction()
        user.position = new_position
        self._users.commit()

    def _resolve_by_email(self, email: str) -> User:
        """
        Returns an instance of user by the specified email.
        Raises an exception otherwise

        :param email: email of user to be fetched
        :return: an instance of User
        :raises UnresolvedEntityError: if user with the
                specified email was not found
        """
        user = self._users.find_by_email(email)

        self._check_resolved(user)

        return user

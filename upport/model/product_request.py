# Include standard modules
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.request import Request
from upport.model.customer import Customer
from upport.model.product import Product
from upport.model.release import Release

from upport.utils.check_instance_or_none import isinstance_or_none


class ProductRequest(Request):
    """
    ProductRequest is a derivative of the Request which has a product
    and target product release associated with it.
    """
    def __init__(self, domain_id: UUID, creator: Customer, title: str, description: str, product: Product = None):
        """
        Constructor. Creates an instance of ProductRequest with an associated product
        but without an associated target release.

        :param domain_id: a unique identifier of this Entity
        :param creator: client, the creator of this Request
        :param title: the title of request
        :param description: the description or text body of the request
        :param product: the product that this request is related to
        """
        super().__init__(domain_id, creator, title, description)

        if not isinstance_or_none(product, Product):
            raise TypeError()

        self._product = product  # type: Product or None
        self._target_release = None  # type: Release or None

    @property
    def product(self) -> Product or None:
        """
        Returns a link to the product which this issue was associated with

        This field can be unset (i.e. equal to None)

        :return: an instance of Product or None if it wasn't set
        """
        return self._product

    @product.setter
    def product(self, new_product: Product or None) -> None:
        """
        Allows to change an associated product

        :param new_product: new product to be associated with
        :return: None
        """
        if not isinstance_or_none(new_product, Product):
            raise TypeError()

        self._product = new_product

    @property
    def target_release(self) -> Release or None:
        """
        Returns a link to the release which will solve this Request
        (implement the feature or fix the bug)

        This field can be unset (i.e. equal to None)

        :return: an instance of Release or None if it wasn't set
        """
        return self._target_release

    @target_release.setter
    def target_release(self, new_release: Release or None) -> None:
        """
        Allows to set a release which will solve this Request
        (implement the feature or fix the bug)

        :param new_release: a new release to be set or None to clear it
        :return: None
        """
        if not isinstance_or_none(new_release, Release):
            raise TypeError()

        self._target_release = new_release

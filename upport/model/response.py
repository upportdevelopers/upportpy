# Include standard modules
from uuid import UUID
from datetime import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.user import User
from upport.utils.check_valid_string import check_string, EmptyStringError


class Response(BaseEntity):
    """
    Response is an item of Request-related discussion
    """

    def __init__(self, domain_id: UUID, creator: User, body: str):
        """
        Creates an instance of Response. Creation time is filled
        automatically and is equal to the instance creation time.

        Response can'be changed after construction. And all responses
        belongs exactly to one Request

        :param domain_id: a unique identifier of this Entity
        :param creator: a link to the User which created this response
        :param body: string, a body of the response
        """
        super().__init__(domain_id)

        if not isinstance(creator, User):
            raise TypeError()

        check_string(body)

        self._creator = creator
        self._body = body
        self._time_created = datetime.utcnow()

    @property
    def creator(self) -> User:
        """
        Return a link to user which created this response

        :return: an instance of User
        """
        return self._creator

    @property
    def time_created(self) -> datetime:
        """
        Returns a time of response creation

        :return: an instance of datetime
        """
        return self._time_created

    @property
    def body(self) -> str:
        """
        Returns a text body (content) of the response

        :return: string, a content of response
        """
        return self._body

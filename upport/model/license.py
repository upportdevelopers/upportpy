# Include standard modules
from uuid import UUID
from datetime import datetime, timedelta

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.license_type import LicenseType
from upport.model.customer import Customer


TIMEDELTA_ZERO = timedelta(0)


class ExpirationPrecedesStartError(ValueError):
    """
    Indicates that the expiration date of the license
    precedes the license validity start date
    """
    pass


class LicenseExpiredError(ValueError):
    """
    Means that the license had already expired
    """
    pass


class ProlongationToPastError(ValueError):
    """
    Means that there was an attempt to 'prolong' a license to some
    time earlier than the current date_end value.
    """
    pass


class LicenseCantBeTransferredError(ValueError):
    """
    Indicates that the license already has its owner
    """
    pass


class License(BaseEntity):
    """
    License is a class that stores information about a Product
    usage license for the specific customer.

    Each customer has its own License for each product. And one customer
    can have a several licenses.
    """
    # FIXME: Consider Change: Add a support of unexpirable licenses

    @staticmethod
    def _check_is_datetime(date: datetime) -> None:
        """
        A utility method that just ensures that the date param specified is
        string.

        :param date: an instance to be checked
        :return: None
        :raises TypeError: if a date parameter is not an instance of datetime
        """
        if not isinstance(date, datetime):
            raise TypeError()

    def __init__(self, domain_id: UUID, license_type: LicenseType, date_start: datetime, date_end: datetime):
        """
        Creates a new instance of license

        :param domain_id: a unique identifier of this Entity
        :param license_type: a type of this license
        :param date_start: a date this License starts be valid from
        :param date_end: a date until this License is valid
        :raises TypeError: when a type of at least one of the parameters
                is invalid
        :raises ExpirationPrecedesStartError: when the date_end time
                precedes a date_start time
        """
        super().__init__(domain_id)

        if not isinstance(license_type, LicenseType):
            raise TypeError()

        self._check_is_datetime(date_start)
        self._check_is_datetime(date_end)

        if date_end < date_start:
            raise ExpirationPrecedesStartError()

        self._license_type = license_type
        self._date_start = date_start
        self._date_end = date_end
        self._customer = None  # type: Customer or None

    @property
    def license_type(self) -> LicenseType:
        """
        Returns a type of this license

        :return: an instance of LicenseType
        """
        return self._license_type

    @property
    def date_start(self) -> datetime:
        """
        Returns a start date of this License instance

        :return: an instance of datetime
        """
        return self._date_start

    @property
    def date_end(self) -> datetime:
        """
        Returns an expiration date for this license

        :return: an instance of datetime
        """
        return self._date_end

    @property
    def customer(self) -> Customer or None:
        """
        Returns a user (customer) of this license. Can
        be empty

        :return: an instance of Customer or None if it
                 not set yet
        """
        return self._customer

    @customer.setter
    def customer(self, customer: Customer) -> None:
        """
        Sets a new user (customer) of this license.
        Can be called only once

        :param customer: a new user of this license
        :return: None
        """
        if self._customer is not None:
            raise LicenseCantBeTransferredError()

        if not isinstance(customer, Customer):
            raise TypeError()

        self._customer = customer

    def invalidate_license(self) -> None:
        """
        Sets an expiration date to NOW. Can be called only once.

        :return: None
        :raises LicenseExpiredError: if this license had already expired
        """
        now = datetime.utcnow()

        if now > self._date_end:
            raise LicenseExpiredError()

        self._date_end = now

    def prolong(self, until: datetime) -> None:
        """
        Move an expiration date to the future

        :param until: new expiration date
        :return: None
        :raises ProlongationToPastError: if 'until' value precedes
                an expiration date
        """
        self._check_is_datetime(until)

        if until < self.date_end:
            raise ProlongationToPastError()

        self._date_end = until

    def prolong_on(self, duration: timedelta) -> None:
        """
        Moves an expiration date to the future

        :param duration: a timespan to be added to the current
                         expiration date
        :return: None
        :raises ProlongationToPastError: if 'duration' is negative
        """
        if not isinstance(duration, timedelta):
            raise TypeError()

        if duration < TIMEDELTA_ZERO:
            raise ProlongationToPastError()

        self._date_end += timedelta


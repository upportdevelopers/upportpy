# Include standard modules
from uuid import UUID
from typing import Set, FrozenSet

# Include 3rd-party modules

# Include Upport modules
from upport.model.user import User
from upport.utils.check_valid_string import check_string, EmptyStringError


class StaffMember(User):
    """
    StaffMember. A user of the system that belongs to the workers
    of user support department.
    """
    def __init__(self, domain_id: UUID, full_name: str, email: str, position: str):
        """
        Creates an instance of Customer without a password and a set
        of licenses specified. They are active by default.

        :param domain_id: a unique identifier of this Entity
        :param full_name: name and surname of the user
        :param email: email of the user
        :param position: string, a position of the member in department
               (like 'developer', 'support agent', 'database specialist'
               and so on)
        """
        super().__init__(domain_id, full_name, email)

        check_string(position)

        self._position = position
        self._avatar = None  # type: str or None
        self._is_active = True

    @property
    def position(self) -> str:
        """
        Returns a string representation of the staff member position

        :return: string, a staff member position
        """
        return self._position

    @position.setter
    def position(self, new_position) -> None:
        """
        Sets a new position to the StaffMember

        :param new_position: a new position to be set
        :return: None
        """
        check_string(new_position)

        self._position = new_position


    @property
    def avatar(self) -> str or None:
        """
        Returns a URI to the StaffMember's avatar. Can be null (None)

        :return: string, a URI to avatar
        """
        return self._avatar

    @avatar.setter
    def avatar(self, uri: str or None) -> None:
        """
        Sets a new avatar for user. Or deletes it if uri is None

        :param uri: string, a URI ot user's avatar
        :return: None
        """
        if isinstance(uri, str) or (uri is None):
            self._avatar = uri

    @property
    def is_active(self) -> bool:
        """
        Returns if the StaffMember account is active and can be used
        for logging in.

        :return: True if account is active, False otherwise (i.e. if
                 it is disabled)
        """
        return self._is_active

    def activate(self) -> None:
        """
        Sets an account to the active state

        :return: None
        """
        self._is_active = True

    def deactivate(self) -> None:
        """
        Sets an account to the deactivated state

        :return: None
        """
        self._is_active = False

# Include standard modules
from uuid import UUID
from typing import Set, FrozenSet
from datetime import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.product import Product
from upport.utils.check_valid_string import check_string, EmptyStringError


class AlreadyReleasedError(ValueError):
    """
    It means that the release instance was already released
    and can't be released again
    """
    pass


class Release(BaseEntity):
    """
    Subsystem is a class that stores information about a some Product's
    subsystem (or module)
    """

    def __init__(self, domain_id: UUID, release_version: str, product: Product):
        """
        Creates an instance of Release with an empty release_date and
        release_title properties. By default is_released property is
        set to False.

        :param domain_id: a unique identifier of this Entity
        :param release_version: string (for now), a version number of
               the product this release is related to
        :param product: a product this release is related to
        """
        super().__init__(domain_id)

        check_string(release_version)

        if not isinstance(product, Product):
            raise TypeError()

        self._version = release_version
        self._date = None  # type: datetime or None
        self._title = None  # type: str or False
        self._is_released = False
        self._product = product

    @property
    def version(self):
        """
        Returns a Product version associated with this release

        :return: string (for now), a version number like v0.1.1
        """
        return self._version

    @property
    def release_date(self) -> datetime or None:
        """
        Returns the date when this release must to be released
        (sorry for tautology). Can be unset (i.e. null, None)

        :return: datetime or None if date wasn't set
        """
        return self._date

    @release_date.setter
    def release_date(self, new_date: datetime or None) -> None:
        """
        Allows to change a date of the release. The date can be
        cleaned (removed) by passing a None parameter value

        :param new_date: new date to be set
        :return: None
        """
        # FIXME: Consider Change: Notify all subscribers on updated release date
        if not isinstance(new_date, datetime) and (new_date is not None):
            raise TypeError()

        self._date = new_date

    @property
    def release_title(self) -> str or None:
        """
        Returns some title (codename, for example, like Spherical Cow
        in Fedora) of the release

        :return: a title of this release or None if it wasn't set
        """
        return self._title

    @release_title.setter
    def release_title(self, new_title: str or None) -> None:
        """
        Allows to set a new title for this release. The title can be
        cleaned (removed) by passing a None parameter value

        :param new_title: a new title of this release
        :return: None
        """
        if new_title is not None:
            check_string(new_title)

        self._title = new_title

    @property
    def is_released(self) -> bool:
        """
        Returns a release status: if it is released

        :return: True if release was released, False otherwise
        """
        return self._is_released

    @property
    def product(self) -> Product:
        """
        Returns an link to Product this release is related to

        :return: an instance of Product
        """
        return self._product

    def mark_released(self) -> None:
        """
        Mark this release as released. This action can be performed only once

        :return: None
        :raises AlreadyReleasedError: if this release was already marked as
                released.
        """
        if self._is_released:
            raise AlreadyReleasedError()

        self._is_released = True

# Include standard modules
from uuid import UUID
from typing import Set, FrozenSet
from enum import Enum
from datetime import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.user import User
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember
from upport.model.response import Response
from upport.model.request_history_item import HistoryItem
from upport.utils.check_valid_string import check_string, EmptyStringError


class RequestStatus(Enum):
    Open = 1,
    Pending = 0,
    Closed = -1


class Request(BaseEntity):
    """
    Request is a sort of basic concept in Upport. It is a some Request
    or Ticket, created by Client to get a solution for his or her needs.
    """
    def __init__(self, domain_id: UUID, creator: Customer, title: str, description: str):
        """
        Constructor. Creates an instance of Request without any
        update history items or responses included and StaffMembers
        assigned. Default status is Open. Creation time field is filled
        automatically and is equal to the construction timestamp.

        :param domain_id: a unique identifier of this Entity
        :param creator: client, the creator of this Request
        :param title: the title of request
        :param description: the description or text body of the request
        """
        super().__init__(domain_id)

        if not isinstance(creator, Customer):
            raise TypeError()

        check_string(title)
        check_string(description)

        self._status = RequestStatus.Open
        self._creator = creator
        self._assignee = None  # type: StaffMember or None
        self._title = title
        self._description = description
        self._time_created = datetime.utcnow()
        self._time_updated = self._time_created

        self._responses = set()  # type: Set[Response]
        self._updates = set()  # type: Set[HistoryItem]

        self._related_requests = set()  # type: Set[Request]

    def _on_property_changed(self, property: str, old_value: str, new_value: str):
        """
        Method to be called on any changes of any properties

        Creates an instance of

        :param property: an identifier of the property that was changed
        :param old_value: old value of this property
        :param new_value: new value of this property
        :return: None
        """
        # TODO: Consider Change: Split request update history logic to the separate service
        self._time_updated = datetime.utcnow()

        # TODO: Add a new history item on change???

    @property
    def status(self) -> RequestStatus:
        """
        Returns a status of the request

        :return: enum value of RequestStatus type
        """
        return self._status

    @status.setter
    def status(self, new_status: RequestStatus) -> None:
        """
        Sets a new status of the request

        :param new_status: new status to be set
        :return: None
        """
        if not isinstance(new_status, RequestStatus):
            raise TypeError()

        self._on_property_changed("status", self.status.name, new_status.name)

        self._status = new_status

    @property
    def creator(self) -> Customer:
        """
        Returns a link to the Customer which created this Request

        :return: an instance of Customer
        """
        return self._creator

    @property
    def assignee(self) -> StaffMember or None:
        """
        Returns a link to the StaffMember which is associated with
        this request and is responsible for it. Can be null (None)
        if no StaffMember was assigned for this Request.

        :return: an instance of StaffMember or None
        """
        return self._assignee

    def assign(self, assignee: StaffMember) -> None:
        """
        (Re)assigns this Request to the new StaffMember

        :param assignee: a person this Request will be assigned to
        :return: None
        """
        # FIXME: Review on_property_changed parameters

        if not isinstance(assignee, StaffMember):
            raise TypeError()

        old_value = None if self.assignee is None else self._assignee.domain_id

        assert assignee is not None

        self._on_property_changed(
            "assignee",
            str(old_value),
            str(assignee.domain_id)
        )

        self._assignee = assignee

    @property
    def title(self) -> str:
        """
        Returns a title of this Request

        :return: string, Request's title
        """
        return self._title

    def update_title(self, new_title: str) -> None:
        """
        Updates the title of this task

        :param new_title: string, a new title to be set
        :return: None
        """
        check_string(new_title)

        self._on_property_changed("title", self.title, new_title)

        self._title = new_title

    @property
    def description(self) -> str:
        """
        Returns a description of the Request. I.e. it returns a text
        representation of the Request body.

        :return: string, a Request's description
        """
        return self._description

    def update_description(self, new_description: str) -> None:
        """
        Updates the description of this task

        :param new_description: string, a new description to be set
        :return: None
        """
        check_string(new_description)

        self._on_property_changed("description", self.description, new_description)

        self._description = new_description

    @property
    def responses(self) -> FrozenSet[Response]:
        """
        Returns a collection of responses (comments) to this Request

        :return: a read-only set of Responses
        """
        return frozenset(self._responses)

    def add_response(self, response: Response) -> None:
        """
        Adds a response to the Request's discussion

        :param response: a new response to be added
        :return: None
        """
        if not isinstance(response, Response):
            raise TypeError()

        # FIXME: Review history items creation for new requests
        self._on_property_changed("requests", None, str(Response))

        self._responses.add(response)

    @property
    def time_created(self) -> datetime:
        """
        Returns the time of the Request creation in the system

        :return: datetime, a creation timestamp
        """
        return self._time_created

    @property
    def time_updated(self) -> datetime:
        """
        Returns the time of the last update of Request fields

        :return: datetime, an update timestamp
        """
        return self._time_updated

    @property
    def updates(self) -> FrozenSet[HistoryItem]:
        """
        Returns a collection of Request's update history

        :return: a read-only set of HistoryItems
        """
        return frozenset(self._updates)

    @property
    def related_requests(self) -> FrozenSet:
        """
        Returns a collection of links to related requests

        :return: a read-only set of Requests
        """
        return frozenset(self._related_requests)

    def add_related(self, request) -> bool:
        """
        Adds a related request

        This method causes a cross-reference of requests. And after
        execution of this method both requests will have cross links
        on each other.

        :param request: an instance of Request
        :return: True if the request was added to the list,
                 False if it was already present in the list of related
        """
        assert request is not self

        if not isinstance(request, Request):
            raise TypeError()

        if request in self._related_requests:
            return False  # do nothing if this request was already added

        self._related_requests.add(request)
        request.add_related(self)

        return True

    def remove_related(self, request) -> bool:
        """
        Remove a request from a list of related requests

        This method cause a cross-deletion of references to requests. And
        after execution of this method both requests will lost their links
        to each other.

        :param request: request to be removed
        :return: True if the request was found and deleted, False otherwise
        """
        assert request is not self

        if not isinstance(request, Request):
            raise TypeError()

        if request not in self._related_requests:
            return False

        self._related_requests.remove(request)
        request.remove_related(self)

        return True

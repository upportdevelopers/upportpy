# Include standard modules
from uuid import UUID
from typing import Set, FrozenSet

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.product import Product
from upport.model.staff_member import StaffMember
from upport.utils.check_valid_string import check_string, EmptyStringError


class Subsystem(BaseEntity):
    """
    Subsystem is a class that stores information about a some Product's
    subsystem (or module)
    """
    def __init__(self, domain_id: UUID, title: str, product: Product):
        """
        Creates an instance of Subsystem with an empty set of related
        developers

        :param domain_id: a unique identifier of this Entity
        :param title: a title of this subsystem (like 'simulation')
        :param product: a product this subsystem is related to
        """
        super().__init__(domain_id)

        check_string(title)

        if not isinstance(product, Product):
            raise TypeError()

        self._product = product
        self._title = title
        self._related_developers = set()  # type: Set[StaffMember]

    @property
    def title(self) -> str:
        """
        A title of this subsystem (like 'simulation' in HDL IDEs)

        :return: string, the title of subsystem
        """
        return self._title

    @title.setter
    def title(self, new_title: str):
        """
        Allows to update a title of this subsystem

        :param new_title: new title
        :return: None
        """
        check_string(new_title)

        self._title = new_title

    @property
    def product(self) -> Product:
        """
        Returns a link to the instance of product this subsystem is related to

        :return: an instance of Product
        """
        return self._product

    @property
    def related_developers(self) -> FrozenSet[StaffMember]:
        """
        Returns a collection of links to the developers that are related to
        and responsible for this Subsystem

        :return: a read-only set of StaffMembers
        """
        return self._related_developers

    # FIXME: Implement an addition of developers!!!!!

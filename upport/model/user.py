# Include standard modules
from uuid import UUID

# Include 3rd-party modules
import passlib
from passlib.ifc import PasswordHash
from passlib.hash import pbkdf2_sha256

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.utils.check_valid_string import check_string, EmptyStringError

# Specify used hasher
Hasher = pbkdf2_sha256  # type: PasswordHash

# Patch old versions of passlib
if passlib.__version__ < '1.7':
    Hasher.hash = Hasher.encrypt


# TODO: A normal implementation of random token fetching
def get_token() -> str:
    return "abcdef"


class InvalidPasswordError(ValueError):
    """
    Indicates that the password specified is invalid and
    doesn't correspond to this user.
    """
    pass


class PasswordNotSetError(ValueError):
    """
    Indicates that the user doesn't yet have a password
    """
    pass


class InvalidResetTokenError(ValueError):
    """
    Indicates that the token specified is invalid and
    doesn't correspond to this user and can't be used
    fot setting of new password.
    """
    pass


class User(BaseEntity):
    """
    User class is an abstraction of a registered user in Upport system
    """
    def __init__(self, domain_id: UUID, full_name: str, email: str):
        """
        Creates an instance of User without a password specified.

        To set a password for user, get a password reset token
        from get_password_token_method and call set_password method
        after that.

        User can't log in to the system if password is not specified.

        :param domain_id: a unique identifier of this Entity
        :param full_name: name and surname of the user
        :param email: email of the user
        """
        super().__init__(domain_id)

        self._full_name = None  # type: str
        self._email = None  # type: str

        self.full_name = full_name
        self.email = email

        self._password_hash = None  # type: str or None
        self._reset_token = None  # type: str or None

    @property
    def full_name(self) -> str:
        """
        Full name (first name + surname) of the user

        :return: string with a user's full name
        """
        return self._full_name

    @full_name.setter
    def full_name(self, new_name: str) -> None:
        """
        Sets a new full name for this user

        :param new_name: new name to be set
        :return: None
        :raises EmptyStringError: when the string specified is empty and thus invalid
        """
        check_string(new_name)

        self._full_name = new_name

    @property
    def email(self) -> str:
        """
        Email address of the user

        :return: string with a user's email
        """
        return self._email

    @email.setter
    def email(self, new_email: str) -> None:
        """
        Sets a new full name for this user

        :param new_name: new name to be set
        :return: None
        :raises EmptyStringError: when the string specified is empty and thus invalid
        """
        check_string(new_email)

        self._email = new_email

    @property
    def is_password_set(self):
        """
        Returns either this user have a valid password set

        :return: True if this user have a password set, False otherwise
        """
        return self._password_hash is not None

    def verify_password(self, password: str) -> bool:
        """
        Checks if the specified password corresponds to this user

        :param password: password to be checked
        :return: True if password is correct, False otherwise
        :raises PasswordNotSetError: when password for this user isn't set at all
        """
        if not self.is_password_set:
            raise PasswordNotSetError()

        return Hasher.verify(password, self._password_hash)

    def get_password_reset_token(self) -> str:
        """
        Allows to get a password reset token

        :return: a password reset token
        """
        if self._reset_token is None:
            self._reset_token = get_token()

        assert self._reset_token is not None

        return self._reset_token

    def invalidate_reset_tokens(self) -> None:
        """
        Allows to invalidate all previously fetched password reset tokens

        :return: None
        """
        self._reset_token = None

    def reset_password(self) -> str:
        """
        Reset a current user password and get an password setting token

        :return: a password setting token
        """
        self._password_hash = None

        return self.get_password_reset_token()

    def change_password(self, old_password: str, new_password: str) -> None:
        """
        Sets a new password for user

        :param old_password: old password of this user
        :param new_password: new password to be set
        :return: None
        :raises PasswordNotSetError: when password for this user isn't set at all
        :raises InvalidPasswordError: when old password is invalid
        """
        if self.verify_password(old_password):
            self.__save_new_password(new_password)

        else:
            raise InvalidPasswordError()

    def set_password(self, reset_token: str, new_password: str) -> None:
        """
        Sets a new password with a reset token specified

        :param reset_token: reset token to be for authorization of password setting
        :param new_password: new password to be set
        :return: None
        :raises InvalidResetTokenError: when the reset token is invalid
        """
        if reset_token != self._reset_token or reset_token is None:
            raise InvalidResetTokenError()

        if reset_token == self._reset_token:
            self.__save_new_password(new_password)
            self._reset_token = None

    def __save_new_password(self, new_password: str) -> None:
        """
        Private method. Updates a password hash of this user

        :param new_password: new password to be saved
        :return: None
        """
        self._password_hash = Hasher.hash(new_password)

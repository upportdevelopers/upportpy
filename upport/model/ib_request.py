# Include standard modules
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.product_request import ProductRequest
from upport.model.customer import Customer
from upport.model.product import Product, UnrelatedSubsystemError
from upport.model.subsystem import Subsystem


class IBRequest(ProductRequest):
    """
    IBRequest of Incorrect Behaviour Request is a Product
    Request that is related to the incorrect behaviour of
    the system in some circumstances. Or, simply, a bug.
    At least, on user's opinion :)
    """
    def __init__(self, domain_id: UUID, creator: Customer, title: str, description: str, product: Product = None):
        """
        Constructor. Creates an instance of IBRequest without an associated subsystems

        :param domain_id: a unique identifier of this Entity
        :param creator: client, the creator of this Request
        :param title: the title of request
        :param description: the description or text body of the request
        :param product: the product that this request is related to
        """
        super().__init__(domain_id, creator, title, description, product)

        self._subsystem = None  # type: Subsystem or None

    @property
    def subsystem(self) -> Subsystem or None:
        """
        The product's subsystem that this request is related to

        :return: an instance of Subsystem or None if it wasn't set
        """
        return self._subsystem

    @subsystem.setter
    def subsystem(self, new_subsystem: Subsystem or None) -> None:
        """
        Allows to change a related subsystem

        :param new_subsystem: a new subsystem to be set or None to clear it
        """
        if new_subsystem is None:
            self.subsystem = new_subsystem
            return

        if not isinstance(new_subsystem, Subsystem):
            raise TypeError()

        if new_subsystem.product is not self.product:
            raise UnrelatedSubsystemError()

        self.subsystem = new_subsystem

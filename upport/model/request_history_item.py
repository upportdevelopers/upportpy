# Include standard modules
from uuid import UUID
from datetime import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.user import User
from upport.utils.check_valid_string import check_string, EmptyStringError


class HistoryItem(BaseEntity):
    """
    HistoryItem contains an information about changes in Request's fields.
    An instance of HistoryItem must be created on each update of the Request.
    """
    def __init__(self, domain_id: UUID, creator: User, field_updated: str, old_value: str, new_value: str):
        """
        Constructor. Contains an information about:

        - who changed request
        - what was changed
        - when the change was performed
        - the previous value of the field
        - the new value

        :param domain_id: a unique identifier of this Entity
        :param creator: a User who made this change
        :param field_updated: a field or property that was updated
        :param old_value: an old value of the property
        :param new_value: a new value of the property
        """
        super().__init__(domain_id)

        if not isinstance(creator, User):
            raise TypeError()

        check_string(field_updated)
        check_string(old_value)
        check_string(new_value)

        self._creator = creator
        self._field_updated = field_updated
        self._old_value = old_value
        self._new_value = new_value

        self._time_created = datetime.utcnow()

    @property
    def time_created(self) -> datetime:
        """
        Returns a creation time of this HistoryItem. And thus -
        the moment of change in Request data.

        :return: datetime, timestamp of the change
        """
        return self._time_created

    @property
    def creator(self) -> User:
        """
        Returns a link to User which performed this change

        :return: an instance of User
        """
        return self._creator

    @property
    def field_updated(self) -> str:
        """
        A string representation of updated field identifier.

        :return: a string, usually a name of updated field
        """
        return self._field_updated

    @property
    def old_value(self) -> str:
        """
        Returns a string representation of the old field value

        :return: string, an old value
        """
        return self._old_value

    @property
    def new_value(self) -> str:
        """
        Returns a string representation of the new field value

        :return: string, an new value
        """
        return self._new_value

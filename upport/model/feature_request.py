# Include standard modules
from uuid import UUID
from typing import Set

# Include 3rd-party modules

# Include Upport modules
from upport.model.product_request import ProductRequest
from upport.model.customer import Customer
from upport.model.product import Product
from upport.model.subsystem import Subsystem


class CustomerNotEndorsedError(KeyError):
    """
    Indicates that the specified customer hadn't endorsed
    this request previously
    """
    pass


class FeatureRequest(ProductRequest):
    """
    FeatureRequest is a request on implementation of some new feature
    in the product.
    """
    def __init__(self, domain_id: UUID, creator: Customer, title: str, description: str, product: Product = None):
        """
        Constructor. Creates an instance of FeatureRequest without
        endorsements

        :param domain_id: a unique identifier of this Entity
        :param creator: client, the creator of this Request
        :param title: the title of request
        :param description: the description or text body of the request
        :param product: the product that this request is related to
        """
        super().__init__(domain_id, creator, title, description, product)

        self._endorsers = set()  # type: Set[Customer]

    @property
    def endorsement_count(self) -> int:
        """
        A number of endorsements that was given to this request

        :return: int, a number of endorsements
        """
        return len(self._endorsers)

    def is_endorsed_by(self, customer: Customer) -> bool:
        """
        Checks if the specified customer endorsed this issue

        :param customer: customer to bec checked
        :return: True is customer endorsed this issue, False otherwise
        """
        return customer in self._endorsers

    def endorse(self, from_customer: Customer) -> None:
        """
        Adds a customer to the internal lists of endorsers

        :param from_customer: a customer which endorsed this issue
        :return: None
        """
        if not isinstance(from_customer, Customer):
            raise TypeError()

        self._endorsers.add(from_customer)

    def remove_endorsement(self, from_customer: Customer) -> None:
        """
        Removes an endorsement from the specified customer

        :param from_customer: customer which previously endorsed this issue
        :return: None
        :raises CustomerNotEndorsedError: if customer hadn't endorsed this issue
                previously
        """
        if not isinstance(from_customer, Customer):
            raise TypeError()

        if not self.is_endorsed_by(from_customer):
            raise CustomerNotEndorsedError()

        self._endorsers.remove(from_customer)

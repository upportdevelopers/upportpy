# Include standard modules
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.user import User


class Customer(User):
    """
    Just a customer. A user of the system that owns some set of
    licenses and posts some requests.
    """
    def __init__(self, domain_id: UUID, full_name: str, email: str):
        """
        Creates an instance of Customer without a password and a set
        of licenses specified.

        :param domain_id: a unique identifier of this Entity
        :param full_name: name and surname of the user
        :param email: email of the user
        """
        super().__init__(domain_id, full_name, email)

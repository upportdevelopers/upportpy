# Include standard modules
from uuid import UUID
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.dtos.product_dto import ProductDto

from .abs_domain_entity_service import AbsDomainEntityService


class AbsProductService(AbsDomainEntityService[ProductDto]):
    """
    This type of service is responsible for Product management:

    - creation of new products
    - altering of product properties

    In future:

    - managing Releases
    - managing Subsystems
    """
    def create_product(self, product_name: str, product_page: str or None) -> UUID:
        """
        Allows to create a new instance of product

        :param product_name: the name of new product
        :param product_page: the product page, may be null (None)
        :return: an identifier of created product
        """
        raise NotImplementedError()

    def set_product_page(self, domain_id: UUID, new_page: str or None) -> None:
        """
        Allows to set a new product page or clean the value of this field

        :param domain_id: an ID of Product to be modified
        :param new_page: a URL to the product new page to be set
        :return: None
        """
        raise NotImplementedError()

    def fetch_by_name(self, product_name: str) -> ProductDto:
        """
        Returns a first found instance of Product DTO that has the specified
        name.

        :param product_name: a name of the product to be found
        :return: an instance of Product DTO
        """
        raise NotImplementedError()


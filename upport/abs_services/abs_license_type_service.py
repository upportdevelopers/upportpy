# Include standard modules
from uuid import UUID
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.dtos.license_type_dto import LicenseTypeDto

from .abs_domain_entity_service import AbsDomainEntityService


class AbsLicenseTypeService(AbsDomainEntityService[LicenseTypeDto]):
    """
    This type of service is responsible for LicenseType management:

    - creation of new LicenseTypes (product editions)
    - altering of LicenseTYpe properties
    """
    # FIXME: Consider Change: Merge with the ProductService

    def create_license_type(self, title: str, product_id: UUID) -> UUID:
        """
        Allows to create a new instance of LicenseType

        :param title: the name of new LicenseType
        :param product_id: an identifier of Product the new LicenseType
               must be related to
        :return: an identifier of the created LicenseType
        """
        raise NotImplementedError()

    def set_description(self, domain_id: UUID, new_description: str or None) -> None:
        """
        Allows to set, unset or change the description of this LicenseType

        :param domain_id: an ID of the LicenseType to be edited
        :param new_description: a new value of description field
        :return: None
        """
        raise NotImplementedError()

    def view_by_product(self, product_id: UUID) -> Sequence[UUID]:
        """
        Returns a collection of LicenseTypes IDs for the specified
        product

        :param product_id: a product the result types must be related to
        :return: a collection of LicenseType IDs
        """
        raise NotImplementedError()



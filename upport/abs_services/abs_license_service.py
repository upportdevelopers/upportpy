# Include standard modules
from uuid import UUID
from typing import Sequence
from datetime import datetime

# Include 3rd-party modules

# Include Upport modules
from upport.dtos.license_dto import LicenseDto

from .abs_domain_entity_service import AbsDomainEntityService


class AbsLicenseService(AbsDomainEntityService[LicenseDto]):
    """
    This type of service is responsible for License management:

    - creation of new Licenses for users
    - revocation of licenses
    - prolongation of licenses
    - various fetching and filtering
    """
    def create_license(self, for_user: UUID, of_type: UUID, from_date: datetime, to_date: datetime) -> UUID:
        """
        Allows to create a new instance of License for User

        :param for_user: an identifier of Customer which will use the new license
        :param of_type: an identifier of LicenseType
        :param from_date: license activity start date
        :param to_date: a date of expiration
        :return: an identifier of the created License
        """
        raise NotImplementedError()

    def prolong_license(self, domain_id: UUID, until: datetime) -> None:
        """
        Allows to postpone a license expiration date

        :param domain_id: an ID of License to be altered
        :param until: a new expiration date in future
        :return: None
        """
        raise NotImplementedError()

    def revoke_license(self, domain_id: UUID) -> None:
        """
        Allows to revoke the specified license

        :param domain_id: an ID of License to be revoked
        :return: None
        """
        raise NotImplementedError()

    def fetch_by_user(self, customer_id: UUID) -> Sequence[UUID]:
        """
        Returns a list of all IDs of Licenses that are used by the
        specified customer

        :param customer_id: an ID of customer to be fetched by
        :return: a collection of IDs of Licenses that are used by Customer
        """
        raise NotImplementedError()

    def view_by_product(self, product_id: UUID) -> Sequence[UUID]:
        """
        Returns a list of all IDs of Licenses that are related to the
        specified Product

        :param product_id: an identifier of Product which Licenses must to belong to
        :return: a list of IDs of issued Licenses for on Product
        """
        raise NotImplementedError()





# Include standard modules
from typing import TypeVar, Generic, Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.dtos.entity_dto import EntityDto


TEntityDto = TypeVar("TEntityDto", bound=EntityDto)


class AbsDomainEntityService(Generic[TEntityDto]):
    """
    Domain Entity Service implements two basic operations for all
    Entities in the system:

    - fetching a full list of identifiers of the specified
      Entity subtype
    - fetching of data transfer object for an Entity by its ID
    """
    def view_all(self) -> Sequence[UUID]:
        """
        Fetch a full list of identifiers of stored objects

        :return: a collection of UUIDs
        """
        raise NotImplementedError()

    def view(self, domain_id: UUID) -> TEntityDto:
        """
        Fetch a DTO of stored object by the ID specified

        :param domain_id: id of object to be fetched
        :return: a DTO of stored object
        """
        raise NotImplementedError()


# Include standard modules
from uuid import UUID
from typing import Sequence

# Include 3rd-party modules

# Include Upport modules
from upport.dtos.request_dto import RequestDto
from upport.dtos.response_dto import ResponseDto

from .abs_domain_entity_service import AbsDomainEntityService


class AbsRequestService(AbsDomainEntityService[RequestDto]):
    """
    This type of service is responsible for Request management:

    - creation of new requests (issues, tickets)
    - creation of responses
    - viewing of responses and update history
    - altering of requests
    - fetching of Requests by different params (filters)
    """
    def create_request(self, creator_id: UUID, title: str, description: str) -> UUID:
        """
        Creates a new request in the system with the specified author (creator),
        title and description.

        :param creator_id: an identifier of creator of this request
        :param title: a title of request
        :param description: a description of request
        :return: an ID of the created request
        """
        raise NotImplementedError()

    def assign_request(self, domain_id: UUID, assignee_id: UUID) -> None:
        """
        Allows to set a new assignee of the request specified

        :param domain_id: an ID of request to be edited
        :param assignee_id: an ID of the new assignee to be set
        :return: None
        """
        raise NotImplementedError()

    def change_status(self, domain_id: UUID, new_status: str) -> None:
        """
        Allows to set a new status of the request

        :param domain_id: an ID of request to be edited
        :param new_status: a string representation of the new state
        :return: None
        """
        raise NotImplementedError()

    def change_title(self, domain_id: UUID, new_title: str) -> None:
        """
        Allows to set a new title of the request

        :param domain_id: an ID of request to be edited
        :param new_title: a new title to be set
        :return: None
        """
        raise NotImplementedError()

    def change_description(self, domain_id: UUID, new_description: str) -> None:
        """
        Allows to set a new description of the request

        :param domain_id: an ID of request to be edited
        :param new_description: a new description to be set
        :return: None
        """
        raise NotImplementedError()

    # FIXME: Consider Change: Return something else or None instead of UUID
    def post_response(self, to_request: UUID, creator_id: UUID, body: str) -> UUID:
        """
        Creates a new response in the request specified

        :param to_request: a request this response is related to
        :param creator_id: an identifier of response creator (User)
        :param body: a body of response
        :return: an identifier of RESPONSE in this request
        """
        raise NotImplementedError()

    def add_related(self, domain_id: UUID, new_related: UUID) -> None:
        """
        Adds a new request with the new_related ID to the request with
        the domain_id specified

        :param domain_id: an acceptor of related request
        :param new_related: a request to be added
        :return: None
        """
        raise NotImplementedError()

    def remove_related(self, domain_id: UUID, on_removal: UUID) -> None:
        """
        Removes a request with the on_removal ID from the request with
        the domain_id specified

        :param domain_id: an acceptor of related request
        :param on_removal: a request to be removed from a list of related
        :return: None
        """
        raise NotImplementedError()

    # FIXME: Consider Change: Remove this method
    def fetch_response(self, in_request: UUID, domain_id: UUID) -> ResponseDto:
        """
        Allows to fetch a specific response with identifier domain_id from
        the request with identifier in_request

        :param in_request: an ID of the Request
        :param domain_id: an ID of the response itself
        :return: RequestDto
        """
        raise NotImplementedError()

    def fetch_all_responses(self, in_request: UUID) -> Sequence[ResponseDto]:
        """
        Allows to fetch all responses in the request specified

        :param in_request: request the responses are related to
        :return: a collection of ResponseDto
        """
        raise NotImplementedError()

    # FIXME: Consider Change: Remove this method
    def fetch_by_related_request(self, to_request: UUID) -> Sequence[UUID]:
        """
        Returns a collection of DTOs of all requests related to the specified one

        :param to_request: a request ID by which the selection is performed
        :return: a collection of IDs of related requests
        """
        raise NotImplementedError()

    def fetch_by_user(self, user_id: UUID) -> Sequence[UUID]:
        """
        Returns a collection of identifiers of all requests that are related to
        the specified user (are assigned to it or was created by it)

        :param user_id: an ID of the user which is related to the returned requests
        :return: a collection of IDs of Requests that are related to this User
        """
        raise NotImplementedError()

    def fetch_created(self, by_user: UUID) -> Sequence[UUID]:
        """
        Returns a collection of identifiers of all requests that are assigned to
        the specified user

        :param by_user: an ID of the creator of requests to be fetched
        :return: a collection of IDs of Requests that are created by this User
        """
        raise NotImplementedError()

    def fetch_assigned(self, to_user: UUID) -> Sequence[UUID]:
        """
        Returns a collection of identifiers of all requests that are assigned to
        the specified user

        :param to_user: an ID of the assignee who created requests to be fetched
        :return: a collection of IDs of Requests that are assigned this User
        """
        raise NotImplementedError()

    def view_unassigned_requests(self) -> Sequence[UUID]:
        """
        Allows to get a full list of requests that haven't an assignee yet

        :return: a collection of IDs of unassigned Requests
        """
        raise NotImplementedError()

    def view_recently_updated_requests(self) -> Sequence[UUID]:
        """
        Allows to get a full list of requests that were updated for the last 24 hours

        :return: a collection of IDs of recently updated Requests
        """
        raise NotImplementedError()

    def view_abandoned_requests(self) -> Sequence[UUID]:
        """
        Allows to get a full list of requests that aren't closed yet
        and was updated more than a month ago

        :return: a collection of IDs of abandoned Requests
        """
        raise NotImplementedError()

    def search_by_title(self, search_string: str) -> Sequence[UUID]:
        """
        Allows to get a full list of request whose titles contain the
        specified search string

        :param search_string: a title search string
        :return: a collection of IDs of Requests which contain the
                 specified string in their titles
        """
        raise NotImplementedError()


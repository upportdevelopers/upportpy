# Include standard modules
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules

from upport.dtos.user_dto import UserDto
from upport.dtos.staff_member_dto import StaffMemberDto

from .abs_domain_entity_service import AbsDomainEntityService


class AbsUserService(AbsDomainEntityService[UserDto]):
    """
    This type of service is responsible for User management:

    - creation of new accounts (users)
    - authentication (here - identification)
    - changing values of User's fields
    - account activation and deactivation
    - password resetting
    - receiving of registration invitations
    """
    def identify(self, email: str, password: str) -> UserDto:
        """
        Check if user-password combination is valid an fetch
        a DTO of the corresponding user

        :param email: an email of the user to be identified
        :param password: a password of the user
        :return: an instance of UserDto
        """
        raise NotImplementedError()

    def change_full_name(self, user_id: UUID, new_name  : str) -> None:
        """
        Changes a full name of the specified User

        :param user_id: -//-
        :param new_name: -//-
        :return: None
        """
        raise NotImplementedError()

    def change_email(self, user_id: UUID, new_email: str) -> None:
        """
        Changes an email of the specified User

        :param user_id: -//-
        :param new_email: -//-
        :return: None
        """
        raise NotImplementedError()

    def change_password(self, user_id: UUID, old_password: str, new_password: str) -> None:
        """
        Changes a password of the specified User

        :param user_id: -//-
        :param old_password: -//-
        :param new_password: -//-
        :return: None
        """
        raise NotImplementedError()

    def get_passwd_reset_token(self, email: str) -> str:
        """
        Allows to fetch a password reset token for the specified User

        :param email: an email of the user
        :return: string, a password reset token
        """
        raise NotImplementedError()

    def reset_password(self, email: str, reset_token: str, new_password: str) -> None:
        """
        Allows to reset a current password for the specified User if
        the specified password reset token is valid

        :param email: an email of the user
        :param reset_token: a reset token
        :param new_password: a new password to be set
        :return: None
        """
        raise NotImplementedError()

    def create_customer(self, full_name: str, email: str) -> UUID:
        """
        Allows to create a new Customer in the system. To pick a
        password, the customer must to perform the password resetting
        procedure

        :param full_name: a full name of the customer
        :param email: an email of the customer
        :return: an ID of the created customer
        """
        raise NotImplementedError()

    def create_staff_member(self, full_name: str, email: str, position: str) -> UUID:
        """
        Allows to create a new StaffMember in the system. To pick a
        password, the StaffMember must to perform the password resetting
        procedure.

        :param full_name: a full name of the customer
        :param email: an email of the customer
        :param position: a position of the staff member in the department
        :return: an ID of the created customer
        """
        raise NotImplementedError()

    def activate_staff_account(self, domain_id: UUID) -> None:
        """
        Allows to make a StaffMember account active, i.e. available for
        logging in (if password was previously set, of course)

        :param domain_id: an ID of StaffMember
        :return: None
        """
        raise NotImplementedError()

    def deactivate_staff_account(self, domain_id: UUID) -> None:
        """
        Allows to make a StaffMember account inactive, i.e. unavailable for
        logging in (either the password was previously set or not).
        At the same time the password setting doesn't change.

        :param domain_id: an ID of StaffMember
        :return: None
        """
        raise NotImplementedError()

    def change_staff_account_position(self, user_id: UUID, new_position: str) -> None:
        """
        Sets a new position for the specified StaffMember

        :param user_id: an ID of the StaffMember to be changed
        :param new_position: a new position to be set
        :return: None
        """
        raise NotImplementedError()

    # FIXME: Consider Change: Add license-related methods to this service


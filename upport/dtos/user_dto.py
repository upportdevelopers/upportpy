# Include standard modules
from uuid import UUID


# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class UserDto(EntityDto):
    def __init__(self, domain_id: UUID, full_name: str, email: str):
        super().__init__(domain_id)

        assert isinstance(full_name, str)
        assert isinstance(email, str)

        self._full_name = full_name
        self._email = email

    @property
    def full_name(self) -> str:
        return self._full_name

    @property
    def email(self) -> str:
        return self._email

    @property
    def is_staff(self) -> bool:
        return False

    def to_dict(self):
        base_dict = super().to_dict()

        base_dict.update(
            {
                "full_name": self.full_name,
                "email": self.email,
                "is_staff": self.is_staff
            }
        )

        return base_dict

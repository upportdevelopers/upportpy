# Include standard modules
from uuid import UUID
from datetime import datetime

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class LicenseDto(EntityDto):
    def __init__(self, domain_id: UUID, type_id: UUID, date_start: datetime, date_end: datetime):
        super().__init__(domain_id)

        assert isinstance(date_start, datetime)
        assert isinstance(date_end, datetime)

        self._type_id = type_id
        self._date_start = date_start
        self._date_end = date_end

    @property
    def type_id(self) -> UUID:
        return self._type_id

    @property
    def date_start(self) -> datetime:
        return self._date_start

    @property
    def date_end(self) -> datetime:
        return self._date_end

# Include standard modules
from uuid import UUID
from datetime import datetime

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class ReleaseDto(EntityDto):
    def __init__(self, domain_id: UUID, release_version: str, release_date: datetime,
                 release_title: str or None, product_id: UUID):

        super().__init__(domain_id)

        self._release_version = release_version
        self._release_date = release_date
        self._release_title = release_title
        self._product_id = product_id

    @property
    def release_version(self) -> str:
        return self.release_version

    @property
    def release_date(self) -> datetime:
        return self._release_date

    @property
    def release_title(self) -> str or None:
        return self._release_title

    @property
    def product_id(self) -> UUID:
        return self._product_id

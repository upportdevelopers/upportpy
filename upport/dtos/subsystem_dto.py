# Include standard modules
from uuid import UUID
from typing import Sequence

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class SubsystemDto(EntityDto):
    def __init__(self, domain_id: UUID, title: str, product_id: UUID, related_developers: Sequence[UUID]):
        super().__init__(domain_id)

        self._title = title
        self._product_id = product_id
        self._related_developers = related_developers

    @property
    def title(self) -> str:
        return self._title

    @property
    def product_id(self) -> UUID:
        return self._product_id

    @property
    def related_developers(self) -> Sequence[UUID]:
        return self._related_developers

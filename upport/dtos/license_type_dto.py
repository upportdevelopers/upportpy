# Include standard modules
from uuid import UUID

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class LicenseTypeDto(EntityDto):
    def __init__(self, domain_id: UUID, title: str, product_id: UUID, description: str or None):
        super().__init__(domain_id)

        assert isinstance(title, str)
        assert isinstance(product_id, UUID)

        self._title = title
        self._product_id = product_id
        self._description = description

    @property
    def title(self) -> str:
        return self._title

    @property
    def product_id(self) -> UUID:
        return self._product_id

    @property
    def description(self) -> str:
        return self._description

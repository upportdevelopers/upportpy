# Include standard modules
from uuid import UUID
from datetime import datetime

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class HistoryItemDto(EntityDto):
    def __init__(self, domain_id: UUID, time_created: datetime, creator_id: UUID,
                 field_updated: str, old_value: str, new_value: str):

        super().__init__(domain_id)

        self._time_created = time_created
        self._creator_id = creator_id
        self._field_updated = field_updated
        self._old_value = old_value
        self._new_value = new_value

    @property
    def time_created(self) -> datetime:
        return self._time_created

    @property
    def creator_id(self) -> UUID:
        return self._creator_id

    @property
    def field_updated(self) -> str:
        return self._field_updated

    @property
    def old_value(self) -> str:
        return self._old_value

    @property
    def new_value(self) -> str:
        return self._new_value

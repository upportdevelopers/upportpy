# Include standard modules
from uuid import UUID
from datetime import datetime
from typing import Sequence

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto
from .response_dto import ResponseDto
from .history_item_dto import HistoryItemDto
from .request_dto import RequestDto

from upport.model.request import RequestStatus


class ProductRequestDto(RequestDto):
    def __init__(self, domain_id: UUID, status: RequestStatus, creator_id: UUID, assignee_id: UUID,
                 title: str, description: str, responses: Sequence[ResponseDto],
                 time_created: datetime, time_updated: datetime, updates: Sequence[HistoryItemDto],
                 related_request_ids: Sequence[UUID], product_id: UUID or None, target_release_id: UUID or None):

        super().__init__(
            domain_id, status, creator_id, assignee_id,
            title, description, responses, time_created, time_updated, updates,
            related_request_ids
        )

        self._product_id = product_id
        self._target_release_id = target_release_id

    @property
    def product_id(self) -> str:
        return self._product_id

    @property
    def target_release_id(self) -> UUID:
        return self._target_release_id

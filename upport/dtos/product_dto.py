# Include standard modules
from uuid import UUID

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class ProductDto(EntityDto):
    # FIXME: Consider Change: Add collections of 'LicenseTypeDto's, 'SubsystemDto's
    # and 'ReleaseDto's to ProductDto body

    def __init__(self, domain_id: UUID, product_name: str, product_page: str or None):
        super().__init__(domain_id)

        self._product_name = product_name
        self._product_page = product_page

    @property
    def product_name(self) -> str:
        return self._product_name

    @property
    def product_page(self) -> str:
        return self._product_page

# Include standard modules
from uuid import UUID

# Include 3rd-party modules
# Include Upport modules


class EntityDto(object):
    """
    EntityDto is a base class for all DTOs of Entity
    subtypes in the system

    This DTO contains only one field: a domain ID
    """
    def __init__(self, domain_id: UUID):
        """
        Creates an instance of EntityDto with the specified
        domain ID
        :param domain_id: a value of domain_id field
        """
        assert isinstance(domain_id, UUID)

        self._id = domain_id

    @property
    def domain_id(self):
        return self._id

    # FIXME: Consider removal of this method
    def to_dict(self) -> dict:
        """
        Converts a DTO object into json-serializable dict

        :return: a dict of object properties
        """
        return {"domain_id": self.domain_id.hex}


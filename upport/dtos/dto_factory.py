# Include standard modules
from functools import singledispatch

# Include 3rd-party modules

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.model.user import User
from upport.model.staff_member import StaffMember
from upport.model.response import Response
from upport.model.request_history_item import HistoryItem
from upport.model.request import Request
from upport.model.product_request import ProductRequest
from upport.model.feature_request import FeatureRequest
from upport.model.ib_request import IBRequest
from upport.model.product import Product
from upport.model.release import Release
from upport.model.subsystem import Subsystem
from upport.model.license_type import LicenseType
from upport.model.license import License

from .entity_dto import EntityDto
from .user_dto import UserDto
from .staff_member_dto import StaffMemberDto
from .response_dto import ResponseDto
from .history_item_dto import HistoryItemDto
from .request_dto import RequestDto
from .product_request_dto import ProductRequestDto
from .feature_request_dto import FeatureRequestDto
from .ib_request_dto import IBRequestDto
from .product_dto import ProductDto
from .release_dto import ReleaseDto
from .subsystem_dto import SubsystemDto
from .license_type_dto import LicenseTypeDto
from .license_dto import LicenseDto


@singledispatch
def build_dto(entity_ins: BaseEntity) -> EntityDto:
    """
    Builds a DTO object by the specified instance of
    the Entity class

    This method is overloaded for different subtypes of
    Entity objects

    :param entity_ins: an base instance of Entity that
           is used for creation of DTO object
    :return: an corresponding instance of DTO
    """
    raise NotImplementedError(
        "No builder method found for this type of Entity"
    )


@build_dto.register(User)
def _(user: User) -> UserDto:
    return UserDto(
        domain_id=user.domain_id,
        full_name=user.full_name,
        email=user.email
    )


@build_dto.register(StaffMember)
def _(user_staff: StaffMember) -> UserDto:
    return StaffMemberDto(
        domain_id=user_staff.domain_id,
        full_name=user_staff.full_name,
        email=user_staff.email,
        position=user_staff.position
    )


@build_dto.register(Response)
def _(response: Response) -> ResponseDto:
    return ResponseDto(
        domain_id=response.domain_id,
        body=response.body,
        time_created=response.time_created,
        creator_id=response.creator.domain_id
    )


@build_dto.register(HistoryItem)
def _(update: HistoryItem) -> HistoryItemDto:
    return HistoryItemDto(
        domain_id=update.domain_id,
        time_created=update.time_created,
        creator_id=update.creator.domain_id,
        field_updated=update.field_updated,
        old_value=update.old_value,
        new_value=update.new_value
    )


def _get_request_dto_init_params(request: Request) -> dict:
    if request.assignee is None:
        assignee_id = None
    else:
        assignee_id = request.assignee.domain_id

    responses = [build_dto(r) for r in request.responses]
    updates = [build_dto(u) for u in request.updates]
    related_ids = [r.domain_id for r in request.related_requests]

    return dict(
        domain_id=request.domain_id,
        status=request.status,
        creator_id=request.creator.domain_id,
        assignee_id=assignee_id,
        title=request.title,
        description=request.description,
        responses=responses,
        time_created=request.time_created,
        time_updated=request.time_updated,
        updates=updates,
        related_request_ids=related_ids
    )


@build_dto.register(Request)
def _(request: Request) -> RequestDto:
    return RequestDto(
        **_get_request_dto_init_params(request)
    )


@build_dto.register(ProductRequest)
def _(request: ProductRequest) -> ProductRequestDto:

    if request.target_release is None:
        target_release_id = None
    else:
        target_release_id = request.target_release.domain_id

    return ProductRequestDto(
        **_get_request_dto_init_params(request),
        product_id=request.product.domain_id,
        target_release_id=target_release_id
    )


def _get_product_request_dto_init_params(request: ProductRequest) -> dict:
    if request.target_release is None:
        target_release_id = None
    else:
        target_release_id = request.target_release.domain_id

    result = dict(
        product_id=request.product.domain_id,
        target_release_id=target_release_id
    )

    result.update(
        _get_request_dto_init_params(request)
    )

    return result


@build_dto.register(ProductRequest)
def _(request: ProductRequest) -> ProductRequestDto:
    return ProductRequestDto(
        **_get_product_request_dto_init_params(request)
    )


@build_dto.register(FeatureRequest)
def _(request: FeatureRequest) -> FeatureRequestDto:
    return FeatureRequestDto(
        **_get_product_request_dto_init_params(request),
        endorsement_count=request.endorsement_count
    )


@build_dto.register(IBRequest)
def _(request: IBRequest) -> IBRequestDto:
    if request.subsystem is None:
        subsystem_id = None
    else:
        subsystem_id = request.subsystem.domain_id

    return IBRequestDto(
        **_get_product_request_dto_init_params(request),
        subsystem_id=subsystem_id
    )


@build_dto.register(Product)
def _(product: Product) -> ProductDto:
    return ProductDto(
        domain_id=product.domain_id,
        product_name=product.product_name,
        product_page=product.product_page
    )


@build_dto.register(Release)
def _(release: Release) -> ReleaseDto:
    return ReleaseDto(
        domain_id=release.domain_id,
        release_version=release.version,
        release_date=release.release_date,
        release_title=release.release_title,
        product_id=release.product.domain_id
    )


@build_dto.register(Subsystem)
def _(subsystem: Subsystem) -> SubsystemDto:
    dev_ids = [d.domain_id for d in subsystem.related_developers]

    return SubsystemDto(
        domain_id=subsystem.domain_id,
        title=subsystem.title,
        product_id=subsystem.product.domain_id,
        related_developers=dev_ids
    )


@build_dto.register(LicenseType)
def _(license_type: LicenseType) -> LicenseTypeDto:
    return LicenseTypeDto(
        domain_id=license_type.domain_id,
        title=license_type.title,
        product_id=license_type.product.domain_id,
        description=license_type.description
    )


@build_dto.register(License)
def _(license_ins: License) -> LicenseDto:
    return LicenseDto(
        domain_id=license_ins.domain_id,
        type_id=license_ins.license_type.domain_id,
        date_start=license_ins.date_start,
        date_end=license_ins.date_end
    )


# Include standard modules
from uuid import UUID
from datetime import datetime

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto


class ResponseDto(EntityDto):
    def __init__(self, domain_id: UUID, body: str, time_created: datetime, creator_id: UUID):
        super().__init__(domain_id)

        self._body = body
        self._time_created = time_created
        self._creator_id = creator_id

    @property
    def response_body(self) -> str:
        return self._body

    @property
    def time_created(self) -> datetime:
        return self._time_created

    @property
    def creator_id(self) -> UUID:
        return self._creator_id

    def to_dict(self) -> dict:
        base_dict = super().to_dict()

        base_dict.update(
            {
                "body": self.response_body,
                "time_created": self.time_created.timestamp(),
                "creator_id": self.creator_id.hex
            }
        )

        return base_dict

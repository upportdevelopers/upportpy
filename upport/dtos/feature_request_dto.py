# Include standard modules
from uuid import UUID
from datetime import datetime
from typing import Sequence

# Include 3rd-party modules
# Include Upport modules
from .product_request_dto import ProductRequestDto, RequestStatus, ResponseDto, HistoryItemDto


class FeatureRequestDto(ProductRequestDto):
    def __init__(self, domain_id: UUID, status: RequestStatus, creator_id: UUID, assignee_id: UUID,
                 title: str, description: str, responses: Sequence[ResponseDto],
                 time_created: datetime, time_updated: datetime, updates: Sequence[HistoryItemDto],
                 related_request_ids: Sequence[UUID], product_id: UUID or None, target_release_id: UUID or None,
                 endorsement_count: int):

        super().__init__(
            domain_id, status, creator_id, assignee_id,
            title, description, responses, time_created, time_updated, updates,
            related_request_ids, product_id, target_release_id
        )

        self._endorsement_count = endorsement_count

    @property
    def endorsement_count(self) -> int:
        return self._endorsement_count

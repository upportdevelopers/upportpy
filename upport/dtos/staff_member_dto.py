# Include standard modules
from uuid import UUID

# Include 3rd-party modules
# Include Upport modules
from .user_dto import UserDto


class StaffMemberDto(UserDto):
    def __init__(self, domain_id: UUID, full_name: str, email: str, position: str):
        super().__init__(domain_id, full_name, email)

        assert isinstance(position, str)

        self._position = position

    @property
    def position(self) -> str:
        return self._position

    @property
    def is_staff(self) -> bool:
        return True

    def to_dict(self):
        base_dict = super().to_dict()

        base_dict.update(
            {
                "position": self.position
            }
        )

        return base_dict

# Include standard modules
from uuid import UUID
from datetime import datetime
from typing import Sequence

# Include 3rd-party modules
# Include Upport modules
from .entity_dto import EntityDto
from .response_dto import ResponseDto
from .history_item_dto import HistoryItemDto

from upport.model.request import RequestStatus


class RequestDto(EntityDto):
    def __init__(self, domain_id: UUID, status: RequestStatus, creator_id: UUID, assignee_id: UUID or None,
                 title: str, description: str, responses: Sequence[ResponseDto],
                 time_created: datetime, time_updated: datetime, updates: Sequence[HistoryItemDto],
                 related_request_ids: Sequence[UUID]):

        super().__init__(domain_id)

        self._status = status
        self._creator_id = creator_id
        self._assignee_id = assignee_id
        self._title = title
        self._description = description
        self._responses = responses
        self._time_created = time_created
        self._time_updated = time_updated
        self._updates = updates
        self._related_request_ids = related_request_ids

    @property
    def status(self) -> str:
        return self._status.name

    @property
    def creator_id(self) -> UUID:
        return self._creator_id

    @property
    def assignee_id(self) -> UUID or None:
        return self._assignee_id

    @property
    def title(self) -> str:
        return self._title

    @property
    def description(self) -> str:
        return self._description

    @property
    def responses(self) -> Sequence[ResponseDto]:
        return self._responses

    @property
    def time_created(self) -> datetime:
        return self._time_created

    @property
    def time_updated(self) -> datetime:
        return self._time_updated

    @property
    def updates(self) -> Sequence[HistoryItemDto]:
        return self._updates

    @property
    def related_request_ids(self) -> Sequence[UUID]:
        return self._related_request_ids

    def to_dict(self) -> dict:
        base_dict = super().to_dict()

        base_dict.update(
            {
                "status": self.status,
                "creator_id": self.creator_id.hex,
                "assignee_id": self.assignee_id.hex if self.assignee_id is not None else None,
                "title": self.title,
                "description": self.description,
                "time_created": self.time_created.timestamp(),
                "time_updated": self.time_updated.timestamp()
            }
        )

        return base_dict

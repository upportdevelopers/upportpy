# Include standard modules
from typing import Sequence, Type
from datetime import datetime
from uuid import UUID

# Include 3rd-party modules
from sqlalchemy.orm import with_polymorphic

# Include Upport modules
from upport.model.request import Request
from upport.model.product_request import ProductRequest
from upport.model.product import Product
from upport.model.customer import Customer
from upport.model.user import User
from upport.model.staff_member import StaffMember
from upport.abs_repos.abs_request_repo import AbsRequestRepository
from upport.repos.alchemy.repo_impls.base_repo import BaseRepository, SessionManager


class RequestRepository(BaseRepository[Request], AbsRequestRepository):
    """
    RequestRepository is an implementation of AbsRequestRepository
    """
    def __init__(self, session_manager: SessionManager):
        stored_cls = with_polymorphic(Request, [Request, ProductRequest])

        super().__init__(session_manager, stored_cls)

    def select_by_product(self, product: Product) -> Sequence[UUID]:
        return self._filtered_by_select(_product_id=product.database_id)

    def select_updated_before(self, before: datetime) -> Sequence[UUID]:
        # noinspection PyUnresolvedReferences
        # noinspection PyProtectedMember
        query = self._get_base_id_query().filter(Request._time_updated < before)

        return self._query_to_id_sequence(query)

    def select_by_creator(self, creator: Customer) -> Sequence[UUID]:
        return self._filtered_by_select(_creator_id=creator.database_id)

    # def select_by_subsystem(self, subsystem: Subsystem) -> Sequence[IBRequest]:
    #     pass

    def select_by_user(self, user: User) -> Sequence[UUID]:
        # noinspection PyUnresolvedReferences
        # noinspection PyProtectedMember

        query = self._get_base_id_query().filter(Request._title.like("%{0}%".format(search_string)))

        return self._query_to_id_sequence(query)

    def select_updated_between(self, after: datetime, before: datetime) -> Sequence[UUID]:
        # noinspection PyUnresolvedReferences
        # noinspection PyProtectedMember
        query = self._get_base_id_query().filter(
            and_(Request._time_updated > after, Request._time_updated < before, )
        )

        return self._query_to_id_sequence(query)

    def select_updated_after(self, after: datetime) -> Sequence[UUID]:
        # noinspection PyUnresolvedReferences
        # noinspection PyProtectedMember
        query = self._get_base_id_query().filter(Request._time_updated > after)

        return self._query_to_id_sequence(query)

    def search_by_title(self, search_string: str) -> Sequence[UUID]:
        # noinspection PyUnresolvedReferences
        # noinspection PyProtectedMember
        query = self._get_base_id_query().filter(Request._title.like("%{0}%".format(search_string)))

        return self._query_to_id_sequence(query)

    def select_by_assignee(self, assignee: StaffMember) -> Sequence[UUID]:
        return self._filtered_by_select(_asssignee_id=assignee.database_id)

    # def select_by_release(self, release: Release) -> Sequence[UUID]:
    #     pass

    def select_unassigned_requests(self) -> Sequence[UUID]:
        return self._filtered_by_select(_assignee_id=None)


# Include standard modules
from typing import Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.customer import Customer
from upport.model.license_type import LicenseType
from upport.model.license import License
from upport.abs_repos.abs_license_repo import AbsLicenseRepository
from upport.repos.alchemy.repo_impls.base_repo import BaseRepository, SessionManager


class LicenseRepository(BaseRepository[License], AbsLicenseRepository):
    """
    LicenseRepository is an SQLAlchemy-specific implementation of
    AbsLicenseRepository
    """
    def __init__(self, session_manager: SessionManager):
        super().__init__(session_manager, License)

    def select_by_user(self, customer: Customer) -> Sequence[UUID]:
        """
        Returns a collection of IDs of all licenses that are given to the
        specified Customer

        :param customer: a customer to be checked
        :return: a collection of License IDs that belongs to this Customer
        """
        return self._filtered_by_select(_customer_id=customer.database_id)

    def select_unused(self) -> Sequence[UUID]:
        """
        Returns a collection of IDs of licenses that are not yet associated to
        or activated by any Customer

        :return: a collection of IDs of unused licenses
        """
        return self._filtered_by_select(_customer_id=None)

    def select_by_type(self, license_type: LicenseType) -> Sequence[UUID]:
        """
        Returns a collection of IDs of licenses of the specific type

        :param license_type: license type of Licenses to be fetched
        :return: a collection of IDs of licenses of the specified type
        """
        return self._filtered_by_select(_license_type_id=license_type.database_id)



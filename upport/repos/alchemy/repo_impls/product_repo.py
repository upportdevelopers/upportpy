# Include standard modules

# Include 3rd-party modules

# Include Upport modules
from upport.model.product import Product
from upport.abs_repos.abs_product_repo import AbsProductRepository
from upport.repos.alchemy.repo_impls.base_repo import BaseRepository, SessionManager


class ProductRepository(BaseRepository[Product], AbsProductRepository):
    """
    AbsProductRepository is an abstract class that declares a generic
    interface of repository of (software) products that are supported
    by User Support Department
    """
    def __init__(self, session_manager: SessionManager):
        super().__init__(session_manager, Product)

    def find_by_name(self, product_name: str) -> Product or None:
        """
        Returns the first found instance of Product with the specified name

        :param product_name: the name of product to be fetched
        :return: an instance of Product or None if it wasn't found
        """
        return self._session.query(self._stored_cls).filter_by(_product_name=product_name).one_or_none()


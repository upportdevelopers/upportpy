# Include standard modules
from typing import Sequence
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from upport.model.product import Product
from upport.model.license_type import LicenseType
from upport.abs_repos.abs_license_type_repo import AbsLicenseTypeRepository
from upport.repos.alchemy.repo_impls.base_repo import BaseRepository, SessionManager


class LicenseTypeRepository(BaseRepository[LicenseType], AbsLicenseTypeRepository):
    """
    LicenseTypeRepository is an SQLAlchemy-specific implementation
    AbsLicenseTypeRepository
    """
    def __init__(self, session_manager: SessionManager):
        super().__init__(session_manager, LicenseType)

    def select_by_product(self, product: Product) -> Sequence[UUID]:
        """
        Returns a collection of license types on the specified product

        :param product: a product licenses must to be related to
        :return: a collection of licenses for this product
        """
        return self._filtered_by_select(_product_id=product.database_id)

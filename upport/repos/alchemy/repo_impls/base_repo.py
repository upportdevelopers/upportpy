# Include standard modules
from typing import TypeVar, Generic, Sequence, Type
from uuid import UUID

# Include 3rd-party modules
from sqlalchemy.orm import Session, Query
from sqlalchemy import func

# Include Upport modules
from upport.model.base_entity import BaseEntity
from upport.abs_repos.abs_repo import AbsRepository
from upport.utils.flatten import flatten
from upport.repos.alchemy.session_manager import SessionManager


TEntity = TypeVar("TEntity", bound=BaseEntity)


class BaseRepository(AbsRepository[TEntity]):
    """
    BaseRepository is a base class for all SQLAlchemy-specific
    implementations of Repository
    """
    def __init__(self, session_manager: SessionManager, stored_cls: Type[BaseEntity]):
        """
        Constructs a basic repository

        :param session_manager: a manager for SQLAlchemy Session
        :param stored_cls: a Type of objects to be stored
        """
        self._session_manager = session_manager
        self._stored_cls = stored_cls

    @property
    def _session(self) -> Session:
        """
        Returns a suitable instance of SQLAlchemy Session

        :return: an instance of Session
        """
        return self._session_manager.session

    def count(self) -> int:
        """
        Returns a number of instances stored in repository

        :return: int, a number of instances
        """
        return self._session.query(
            func.count(self._stored_cls._database_id)
        ).scalar()

    def load(self, db_id: int) -> TEntity or None:
        """
        Loads an object with the specified database identifier
        to memory

        :param db_id: int, a database identifier of object
        :return: an instance of object with the corresponding database ID
                 or None if not found
        """
        return self._session.query(self._stored_cls).get(db_id)

    def load_all(self) -> Sequence[TEntity]:
        """
        Returns a collection of all objects stored

        :return: a collection of stored objects
        """
        return self._session.query(self._stored_cls).all()

    def find_by_domain_id(self, domain_id: UUID) -> TEntity or None:
        """
        Returns an Entity instance by the specified
        domain ID of this instance

        :param domain_id: a domain ID of Entity to be found
        :return: an instance of Entity with the specified ID
                 or None if it was not found
        """
        return self._session.query(self._stored_cls).filter_by(_domain_id=domain_id).one_or_none()

    def select_all_domain_ids(self) -> Sequence[UUID]:
        """
        Returns a collection of domain identifiers of all
        Entity instances stored in repository

        :return: a collection of domain identifiers
        """
        query = self._get_base_id_query()
        return self._query_to_id_sequence(query)

    def add(self, new_obj: TEntity) -> None:
        """
        Add a new object to the repository

        :param new_obj: new object to be stored
        :return: None
        """
        self._session.add(new_obj)

    def delete(self, obj: TEntity) -> None:
        """
        Deletes the object specified from the repository

        :param obj: an object to be deleted
        :return: None
        """
        self._session.delete(obj)

    # TODO: Add a context manager for transactions
    def start_transaction(self) -> None:
        """
        Starts a transaction - a set of related changes in
        persistent storage

        :return: None
        """
        # There is no need to start a transaction explicitly if
        # autocommit mode is disabled:
        # http://docs.sqlalchemy.org/en/latest/orm/session_transaction.html#session-autocommit
        pass

    def commit(self) -> None:
        """
        Save all made changes to the persistent storage

        :return: None
        """
        self._session.commit()

    def rollback(self) -> None:
        """
        Rollbacks the current transaction

        :return: None
        """
        self._session.rollback()

    def _build_filter_by_query(self, **kwargs) -> Query:
        """
        Constructs a filter query based on the specified
        filtering criteria.

        This query will return a collection of rows with
        one column only: domain_id

        :param kwargs: criteria for filtering
        :return: an instance of Query
        """
        return self._get_base_id_query().filter_by(**kwargs)

    @staticmethod
    def _query_to_id_sequence(query: Query) -> Sequence[UUID]:
        """
        Returns a sequence of domain identifiers by the specified query

        :param query: query to be executed
        :return: a sequence of IDs from the query
        """
        column_values_collection = query.all()

        return flatten(column_values_collection)

    def _filtered_by_select(self, **kwargs) -> Sequence[UUID]:
        """
        Allows to select identifiers of objects which satisfy
        the specified filter criteria

        :param kwargs: criteria for filtering
        :return: a collection of object IDs
        """
        query = self._build_filter_by_query(**kwargs)

        return self._query_to_id_sequence(query)

    def _get_base_id_query(self) -> Query:
        """
        Returns an instance of Query that fetches only domain_id
        column from the table. It will look like this:

            SELECT class_name._domain_id FROM table_name

        :return: an instance of Query
        """
        return self._session.query(self._stored_cls._domain_id)

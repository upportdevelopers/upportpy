# Include standard modules
from typing import Type

# Include 3rd-party modules
from sqlalchemy.orm import with_polymorphic

# Include Upport modules
from upport.model.user import User
from upport.model.staff_member import StaffMember
from upport.model.customer import Customer
from upport.abs_repos.abs_user_repo import AbsUserRepository
from .base_repo import BaseRepository, SessionManager


class UserRepository(BaseRepository[User], AbsUserRepository):
    """
    AbsUserRepository is an abstract class that declares a generic
    interface of repository of users
    """
    def __init__(self, session_manager: SessionManager):
        stored_cls = with_polymorphic(User, [StaffMember, Customer])

        super().__init__(session_manager, stored_cls)

    def find_by_email(self, email: str) -> User or None:
        """
        Returns an instance of User by his email

        :param email: email of the user to be found
        :return: an instance of User or None if it wasn't found
        """
        return self._session.query(self._stored_cls).filter_by(_email=email).one_or_none()


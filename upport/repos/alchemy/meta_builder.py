from sqlalchemy import \
    MetaData, Table, Column, ForeignKey, Boolean, DateTime, \
    Integer, String, Enum, PickleType

from sqlalchemy.engine import Connectable
from sqlalchemy.orm import mapper, relationship
from sqlalchemy.ext.mutable import MutableSet

from sqlalchemy_utils.types import UUIDType

from upport.model.product import Product
from upport.model.user import User
from upport.model.customer import Customer
from upport.model.staff_member import StaffMember
from upport.model.license_type import LicenseType
from upport.model.license import License
from upport.model.request import Request, RequestStatus
from upport.model.product_request import ProductRequest


class MetaBuilder(object):
    """
    MetaBuilder is a utility class that is responsible for
    building of MetaData object, definition of Tables and their
    structure and mapping of them to the classes via ORM
    """
    def __init__(self):
        self._metadata = MetaData()
        self.create_tables()
        self.create_mappers()

    def create_tables(self) -> None:
        """
        Performs an initiation of all tables and appends them
        to stored metadata

        :return: None
        """
        self._product_table = Table(
            'products', self._metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType(binary=False), unique=True, index=True),
            Column('_product_name', String(50)),
            Column('_product_page', String(50))
        )

        self._user_table = Table(
            'users', self._metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType(binary=False), unique=True, index=True),
            Column('_user_type', String(50)),
            Column('_full_name', String(50)),
            Column('_email', String(50)),
            Column('_password_hash', String(128)),
            Column('_reset_token', String(50))
        )

        self._staff_table = Table(
            'staff_members', self._metadata,
            Column('_database_id', Integer, ForeignKey('users._database_id'), primary_key=True),
            Column('_position', String(50)),
            Column('_avatar', String(50)),
            Column('_is_active', Boolean)
        )

        self._customer_table = Table(
            'customers', self._metadata,
            Column('_database_id', Integer, ForeignKey('users._database_id'), primary_key=True)
        )

        self._license_type_table = Table(
            'license_types', self._metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType(binary=False), unique=True, index=True),
            Column('_product_id', Integer, ForeignKey('products._database_id')),
            Column('_title', String(50)),
            Column('_description', String(200))
        )

        self._license_table = Table(
            'licenses', self._metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType(binary=False), unique=True, index=True),
            Column('_license_type_id', Integer, ForeignKey('license_types._database_id')),
            Column('_date_start', DateTime),
            Column('_date_end', DateTime),
            Column('_customer_id', Integer, ForeignKey('customers._database_id'))
        )

        """
        self._status = RequestStatus.Open
        self._creator = creator
        self._assignee = None  # type: StaffMember or None
        self._title = title
        self._description = description
        self._time_created = datetime.utcnow()
        self._time_updated = self._time_created

        self._responses = set()  # type: Set[Response]
        self._updates = set()  # type: Set[HistoryItem]

        self._related_requests = set()  # type: Set[Request]
        """

        self._request_association_table = Table(
            'request_associations', self._metadata,
            Column('left_id', Integer, ForeignKey('requests._database_id')),
            Column('right_id', Integer, ForeignKey('requests._database_id'))
        )

        self._request_table = Table(
            'requests', self._metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType(binary=False), unique=True, index=True),
            Column('_request_type', String(50)),
            Column('_status', Enum(RequestStatus)),
            Column('_creator_id', ForeignKey('customers._database_id')),
            Column('_assignee_id', ForeignKey('staff_members._database_id')),
            Column('_title', String(50)),
            Column('_description', String(300)),
            Column('_time_created', DateTime),
            Column('_time_updated', DateTime),
            Column('_responses', MutableSet.as_mutable(PickleType)),
            Column('_updates', MutableSet.as_mutable(PickleType))
        )

        self._product_requests_table = Table(
            'product_requests', self._metadata,
            Column('_database_id', Integer, ForeignKey('requests._database_id'), primary_key=True),
            Column('_product_id', ForeignKey('products._database_id'))
        )

    def create_mappers(self) -> None:
        """
        Creates mappers between the object model classes and the
        corresponding tables

        :return: None
        """
        mapper(Product, self._product_table)
        mapper(User, self._user_table, polymorphic_on=self._user_table.c._user_type, polymorphic_identity='user')
        mapper(StaffMember, self._staff_table, inherits=User, polymorphic_identity='staff_member')
        mapper(Customer, self._customer_table, inherits=User, polymorphic_identity='customer')
        mapper(LicenseType, self._license_type_table, properties={
            '_product': relationship(Product)
        })

        mapper(License, self._license_table, properties={
            '_customer': relationship(Customer),
            '_license_type': relationship(LicenseType)
        })

        mapper(
            Request, self._request_table,
            polymorphic_on=self._request_table.c._request_type,
            polymorphic_identity='request', properties={
                '_related_requests': relationship(
                    Request, collection_class=set, secondary=self._request_association_table,
                    primaryjoin=self._request_table.c._database_id==self._request_association_table.c.left_id,
                    secondaryjoin=self._request_table.c._database_id==self._request_association_table.c.right_id,
                ),
                '_assignee': relationship(StaffMember),
                '_creator': relationship(Customer)
            }
        )

        mapper(
            ProductRequest, self._product_requests_table,
            inherits=Request, polymorphic_identity='product_request',
            properties={
                '_product': relationship(Product)
            }
        )

    def create_all_tables(self, bind: Connectable) -> None:
        """
        Calls create_all on the stored metadata. Creates all
        tables in DB if they are not created yet

        :param bind: an instance of connectable for which
               the tables must be created
        :return: None
        """
        self._metadata.create_all(bind=bind)

    def drop_all_tables(self, bind: Connectable) -> None:
        """
        Calls drop_all on the stored metadata. Drops all
        tables in DB if they was created yet

        :param bind: an instance of connectable for which
               the tables must be dropped
        :return: None
        """
        self._metadata.drop_all(bind=bind)

from contextlib import contextmanager

from sqlalchemy.engine import Connectable
from sqlalchemy.orm import Session, sessionmaker


class SessionManager(object):
    """
    SessionManager is responsible for managing of Session
    objects from SQLAlchemy. It contains methods for creating
    of a new Session, and accessing the current session
    """
    def __init__(self, bind: Connectable):
        """
        Initialize SessionManager instance

        :param bind: an instance of Connectable, a connection
               to the database to be used
        """

        self._session_maker = sessionmaker(bind=bind)
        self._current_session = None  # type: Session

    @property
    def session(self) -> Session:
        """
        Contains a reference to the current session

        :return: an instance of SQLAlchemy Session
        """
        if self._current_session is None:
            self._current_session = self._session_maker()

        return self._current_session

    def renew_session(self) -> Session:
        """
        Creates and returns a new instance of session. The
        created instance will also be saved to the 'session'
        property value

        :return: a new instance of Session
        """
        new_session = self._session_maker()

        self._current_session = new_session

        return new_session

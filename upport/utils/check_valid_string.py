class EmptyStringError(ValueError):
    """
    Indicates that the string specified is empty and thus invalid
    """
    pass


def check_string(string: str) -> None:
    """
    Checks if the string specified is valid. Raises exceptions otherwise

    :param string: string to be checked
    :return: None
    :raises EmptyStringError: if string is empty
    :raises TypeError: if the string instance specified is not a string at all
    """
    if not isinstance(string, str):
        raise TypeError()

    if len(string) == 0:
        raise EmptyStringError()

from typing import Type


def isinstance_or_none(o: object, t: Type) -> bool:
    """
    Returns True if object o is instance of t OR
    if object o is None

    :param o: object to be checked
    :param t: expected type of object
    :return: True if o is instance of t or None
    """
    return isinstance(o, t) or (o is None)

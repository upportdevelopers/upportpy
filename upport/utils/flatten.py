import itertools
import typing


def flatten(collection: typing.Iterable) -> typing.Sequence:
    """
    Flattens a collection of iterables, i.e. converts
    (("one",), ("two", ), ("three"),)
    to
    ("one", "two", "three")

    :param collection: a collection to be flattened
    :return: an sequence that represents the results of flattering
    """
    return list(itertools.chain.from_iterable(collection))
